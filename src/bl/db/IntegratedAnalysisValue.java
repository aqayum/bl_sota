package bl.db;

/**
 * @author Abdul
 *
 */
public class IntegratedAnalysisValue {
	//BUG_ID, MTH_ID, VSM_SCORE, SIMI_SCORE, BL_SCORE, COMM_SCORE, MID_MTH_SCORE, BLIA_MTH_SCORE
	protected int bugID;
	protected String methodName;
	protected String version;
	protected int methodID;
	protected double vsmScore;
	protected double vsmClassicScore;


	protected double similarityScore;
	protected double bugLocatorScore;
	protected double commitLogScore;


	/**
	 * 
	 */
	public IntegratedAnalysisValue() {
		bugID = 0;
		methodName = "";
		version = "";
		methodID = -1;
		vsmScore = 0.0;
		similarityScore = 0.0;
		bugLocatorScore = 0.0;
		commitLogScore = 0.0;

	}
	
	public double getVsmClassicScore() {
		return vsmClassicScore;
	}


	public void setVsmClassicScore(double vsmClassicScore) {
		this.vsmClassicScore = vsmClassicScore;
	}
	/**
	 * @return the bugID
	 */
	public int getBugID() {
		return bugID;
	}

	/**
	 * @param bugID the bugID to set
	 */
	public void setBugID(int bugID) {
		this.bugID = bugID;
	}

	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @param methodName the methodName to set
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * @return the vsmScore
	 */
	public double getVsmScore() {
		return vsmScore;
	}

	/**
	 * @param vsmScore the vsmScore to set
	 */
	public void setVsmScore(double vsmScore) {
		this.vsmScore = vsmScore;
	}

	/**
	 * @return the similatiryScore
	 */
	public double getSimilarityScore() {
		return similarityScore;
	}

	/**
	 * @param similarityScore the similarityScore to set
	 */
	public void setSimilarityScore(double similarityScore) {
		this.similarityScore = similarityScore;
	}

	/**
	 * @return the bugLocatorScore
	 */
	public double getBugLocatorScore() {
		return bugLocatorScore;
	}

	/**
	 * @param bugLocatorScore the bugLocatorScore to set
	 */
	public void setBugLocatorScore(double bugLocatorScore) {
		this.bugLocatorScore = bugLocatorScore;
	}



	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the sourceMethodVersionID
	 */
	public int getSourceMethodVersionID() {
		return methodID;
	}

	/**
	 * @param methodID the methodID to set
	 */
	public void setMethodID(int methodID) {
		this.methodID = methodID;
	}
	
	public int getMethodID() {
		return this.methodID;
	}

	/**
	 * @return the commitLogScore
	 */
	public double getCommitLogScore() {
		return commitLogScore;
	}

	/**
	 * @param commitLogScore the commitLogScore to set
	 */
	public void setCommitLogScore(double commitLogScore) {
		this.commitLogScore = commitLogScore;
	}

}
