package bl.db;

/**
 * @author Abdul
 *
 */
public class ExtendedIntegratedAnalysisValue extends IntegratedAnalysisValue {
	private int methodID;
	private double bliaMethodScore;
	
	public ExtendedIntegratedAnalysisValue() {
		super();
		setMethodID(-1);
		setBliaMethodScore(0.0);
	}

	/**
	 * @return the methodID
	 */
	public int getMethodID() {
		return methodID;
	}

	/**
	 * @param methodID the methodID to set
	 */
	public void setMethodID(int methodID) {
		this.methodID = methodID;
	}

	/**
	 * @return the bliaMethodScore
	 */
	public double getBliaMethodScore() {
		return bliaMethodScore;
	}

	/**
	 * @param bliaMethodScore the bliaMethodScore to set
	 */
	public void setBliaMethodScore(double bliaMethodScore) {
		this.bliaMethodScore = bliaMethodScore;
	}
}
