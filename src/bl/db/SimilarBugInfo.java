package bl.db;

/**
 * @author Abdul
 *
 */
public class SimilarBugInfo {
	private int similarBugID;
	private double similarityScore;

	/**
	 * 
	 */
	public SimilarBugInfo() {
		setSimilarBugID(0);
		setSimilarityScore(0.0);
	}

	/**
	 * @return the bugID
	 */
	public int getSimilarBugID() {
		return similarBugID;
	}

	/**
	 * @param bugID the bugID to set
	 */
	public void setSimilarBugID(int bugID) {
		this.similarBugID = bugID;
	}

	/**
	 * @return the similarityScore
	 */
	public double getSimilarityScore() {
		return similarityScore;
	}

	/**
	 * @param similarityScore the similarityScore to set
	 */
	public void setSimilarityScore(double similarityScore) {
		this.similarityScore = similarityScore;
	}

}
