package bl.db;

/**
 * @author Abdul
 *
 */
public class AnalysisValue {
	protected String version;
	protected String term;
	protected int methodID;
	protected int bugID;


	protected int termID;
	protected int termCount;
	protected int invDocCount;
	protected double tfClassic;
	protected double tf;
	protected double idf;
	private double termWeight;
	
	final private int INIT_VALUE = -1;

	public int getBugID() {
		return bugID;
	}
	public double getTfClassic() {
		return tfClassic;
	}

	public void setTfClassic(double tfClassic) {
		this.tfClassic = tfClassic;
	}

	public void setBugID(int bugID) {
		this.bugID = bugID;
	}
	
	public AnalysisValue() {
		version = "";
		term = "";
		methodID = INIT_VALUE;
		bugID = INIT_VALUE;
		setTermID(INIT_VALUE);
		termCount = INIT_VALUE;
		invDocCount = INIT_VALUE;
		tf = INIT_VALUE;
		idf = INIT_VALUE;
		termWeight = INIT_VALUE;
	}
//	
//	public AnalysisValue(String term, double termWeight) {
//		version = "";
//		setTerm(term);
//		setTermID(INIT_VALUE);
//		termCount = INIT_VALUE;
//		invDocCount = INIT_VALUE;
//		tf = INIT_VALUE;
//		idf = INIT_VALUE;
//		setTermWeight(termWeight);
//	}
//	
//	public AnalysisValue(int ID,  double termWeight) {
//
//		version = "";
//		setTerm(term);
//		setMethodID(ID);
//		setTermID(INIT_VALUE);
//		termCount = INIT_VALUE;
//		invDocCount = INIT_VALUE;
//		tf = INIT_VALUE;
//		idf = INIT_VALUE;
//		setTermWeight(termWeight);
//	}
//	
//	public AnalysisValue(String term, int termCount, int invDocCount, double tf, double idf) {
//		version = "";
//		setTerm(term);
//		setTermID(INIT_VALUE);
//		setTermCount(termCount);
//		setInvDocCount(invDocCount);
//		setTf(tf);
//		setIdf(idf);
//		setTermWeight(INIT_VALUE);
//	}
//	
//	public AnalysisValue(int ID, String term, int termCount, int invDocCount, double tf, double idf) {
//		setMethodID(ID);
//		version = "";
//		setTerm(term);
//		setTermID(INIT_VALUE);
//		setTermCount(termCount);
//		setInvDocCount(invDocCount);
//		setTf(tf);
//		setIdf(idf);
//		setTermWeight(INIT_VALUE);
//	}
//	
//	public AnalysisValue(String term, int termCount, int invDocCount, double tf, double idf, double termWeight) {
//		setMethodID(0);
//		version = "";
//		setTerm(term);
//		setTermID(INIT_VALUE);
//		setTermCount(termCount);
//		setInvDocCount(invDocCount);
//		setTf(tf);
//		setIdf(idf);
//		setTermWeight(termWeight);
//	}
//	
//	public AnalysisValue(int ID, String term, int termCount, int invDocCount, double tf, double idf, double termWeight) {
//		setMethodID(ID);
//		version = "";
//		setTerm(term);
//		setTermID(INIT_VALUE);
//		setTermCount(termCount);
//		setInvDocCount(invDocCount);
//		setTf(tf);
//		setIdf(idf);
//		setTermWeight(termWeight);
//	}	
//	
//	/**
//	 * 
//	 */
//	public AnalysisValue(String version, String term, int termCount, int invDocCount) {
//		setMethodID(0);
//		setVersion(version);
//		setTerm(term);
//		setSourceFileVersionID(INIT_VALUE);
//		setTermID(INIT_VALUE);
//		setTermCount(termCount);
//		setInvDocCount(invDocCount);
//		setTf(INIT_VALUE);
//		setIdf(INIT_VALUE);
//		setTermWeight(INIT_VALUE);
//	}
	
	public AnalysisValue(int methodID, String version, String term, int termCount, int invDocCount) {
		this.setMethodID(methodID);

		this.setVersion(version);
		this.setTerm(term);
		this.setTermID(INIT_VALUE);
		this.setTermCount(termCount);
		this.setInvDocCount(invDocCount);
		this.setTf(INIT_VALUE);
		this.setIdf(INIT_VALUE);
		this.setTermWeight(INIT_VALUE);
	}
	
	/**
	 * 
	 */
//	public AnalysisValue( String version, String term, int termCount, int invDocCount, double tf, double idf) {
//		setMethodID(0);
//		setVersion(version);
//		setTerm(term);
//		setSourceFileVersionID(INIT_VALUE);
//		setTermID(INIT_VALUE);
//		setTermCount(termCount);
//		setInvDocCount(invDocCount);
//		setTf(tf);
//		setIdf(idf);
//		setTermWeight(INIT_VALUE);
//	}

	public AnalysisValue(int bugID, String bugTerm, int bugTermCount, int invDocCount, double tf, double idf, double tfClassic) {
		this.setBugID(bugID);
		setTerm(bugTerm);
		setTermCount(bugTermCount);
		setInvDocCount(invDocCount);
		setTf(tf);
		setIdf(idf);
		setTfClassic(tfClassic);
		setTermWeight(INIT_VALUE);
	}
	
	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the termCount
	 */
	public int getTermCount() {
		return termCount;
	}

	/**
	 * @param termCount the termCount to set
	 */
	public void setTermCount(int termCount) {
		this.termCount = termCount;
	}

	/**
	 * @return the invDocCount
	 */
	public int getInvDocCount() {
		return invDocCount;
	}

	/**
	 * @param invDocCount the invDocCount to set
	 */
	public void setInvDocCount(int invDocCount) {
		this.invDocCount = invDocCount;
	}

	/**
	 * @return the tf
	 */
	public double getTf() {
		return tf;
	}

	/**
	 * @param tf the tf to set
	 */
	public void setTf(double tf) {
		this.tf = tf;
	}

	/**
	 * @return the idf
	 */
	public double getIdf() {
		return idf;
	}

	/**
	 * @param idf the idf to set
	 */
	public void setIdf(double idf) {
		this.idf = idf;
	}

	/**
	 * @return the term
	 */
	public String getTerm() {
		return term;
	}

	/**
	 * @param term the term to set
	 */
	public void setTerm(String term) {
		this.term = term;
	}

	/**
	 * @return the sourceFileVersionID
	 */
	public int getMethodID() {
		return methodID;
	}

	/**
	 * @param sourceFileVersionID the sourceFileVersionID to set
	 */


	/**
	 * @return the termID
	 */
	public int getTermID() {
		return termID;
	}

	/**
	 * @param termID the termID to set
	 */
	public void setTermID(int termID) {
		this.termID = termID;
	}

	/**
	 * @return the termWeight
	 */
	public double getTermWeight() {
		return termWeight;
	}

	/**
	 * @param termWeight the termWeight to set
	 */
	public void setTermWeight(double termWeight) {
		this.termWeight = termWeight;
	}
	public void setMethodID(int methodID) {
		this.methodID = methodID;
	}


}
