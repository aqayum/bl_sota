package bl.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.h2.jdbcx.JdbcConnectionPool;

import bl.config.Property;

/**
 * @author Abdul
 *
 */
public class BaseDAO {
	protected static Connection analysisDbConnection = null;
	protected static Connection evaluationDbConnection = null;
	protected PreparedStatement ps = null;
	protected ResultSet rs = null;
	
	final public static int INVALID = -1;
	final static String DEFAULT_DB_NAME = "sample";
	
	public BaseDAO() throws Exception {
		Property property = Property.getInstance(); 
		
		String dbName = BaseDAO.DEFAULT_DB_NAME;
		if (property != null) {
			dbName = property.getProductName();
		}

		openConnection(dbName);
	}

	public static void openEvaluationDbConnection() throws Exception {
		if (null == evaluationDbConnection) {
			Class.forName("org.h2.Driver");
			String connectionURL = "jdbc:h2:file:./db/evaluation";
			JdbcConnectionPool connectionPool = JdbcConnectionPool.create(connectionURL, "sa", "");
			evaluationDbConnection = connectionPool.getConnection();
		}
	}
	
	public static void openConnection(String dbName) throws Exception {
		openEvaluationDbConnection();
		
		if (null == analysisDbConnection) {
			Class.forName("org.h2.Driver");
			String connectionURL = "jdbc:h2:file:./db/" + dbName;
			JdbcConnectionPool connectionPool = JdbcConnectionPool.create(connectionURL, "sa", "");
			analysisDbConnection = connectionPool.getConnection();
		}
	}
	public static void closeConnection() throws Exception {
		if (null != analysisDbConnection) {
			analysisDbConnection.close();
			analysisDbConnection = null;
		}
		
		if (null != evaluationDbConnection) {
			evaluationDbConnection.close();
			evaluationDbConnection = null;
		}
	}
	
	public static Connection getAnalysisDbConnection() {
		return analysisDbConnection;
	}
	
	public static Connection getEvaluationDbConnection() {
		return evaluationDbConnection;
	}
}
