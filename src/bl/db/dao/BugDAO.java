package bl.db.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import org.h2.api.ErrorCode;
import org.h2.jdbc.JdbcSQLException;

import bl.common.Bug;
import bl.common.BugCorpus;
import bl.common.Comment;
import bl.common.Method;
import bl.common.SourceFile;
import bl.db.AnalysisValue;
import bl.db.SimilarBugInfo;

/**
 * @author Abdul
 *
 */
public class BugDAO extends BaseDAO {

	/**
	 * @throws Exception
	 */
	public BugDAO() throws Exception {
		super();
	}
	
	public int insertStructuredBug(Bug bug) {
		String sql = "INSERT INTO BUG_INFO (BUG_ID, OPEN_DATE, FIXED_DATE, SMR_COR, DESC_COR, CMT_COR, TOT_CNT, VER)" +
					 " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		int returnValue = INVALID;
		
		// releaseDate format : "2004-10-18 17:40:00"
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, bug.getID());
			ps.setString(2, bug.getOpenDateString());
			ps.setString(3, bug.getFixedDateString());
			BugCorpus bugCorpus = bug.getCorpus();
			ps.setString(4, bugCorpus.getSummaryPart());
			ps.setString(5, bugCorpus.getDescriptionPart());
			ps.setString(6, bug.getAllCommentsCorpus());
			ps.setInt(7, bug.getTotalCorpusCount());
			ps.setString(8, bug.getVersion());
			
			returnValue = ps.executeUpdate();
			
			
		} catch (JdbcSQLException e) {
			if (ErrorCode.DUPLICATE_KEY_1 != e.getErrorCode()) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		
		return returnValue;
	}
	
	public int deleteAllBugs() {
		String sql = "DELETE FROM BUG_INFO";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public HashMap<Integer, Bug> getBugs() {
		HashMap<Integer, Bug> bugs = new HashMap<Integer, Bug>();
		
		String sql = "SELECT BUG_ID, OPEN_DATE, FIXED_DATE, SMR_COR, DESC_COR, TOT_CNT, COR_NORM, SMR_COR_NORM, DESC_COR_NORM, VER FROM BUG_INFO";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			Bug bug = null;
			rs = ps.executeQuery();
			while (rs.next()) {
				bug = new Bug();
				bug.setID(rs.getInt("BUG_ID"));
				bug.setOpenDate(rs.getTimestamp("OPEN_DATE"));
				bug.setFixedDate(rs.getTimestamp("FIXED_DATE"));
				
				BugCorpus bugCorpus = new BugCorpus();
				bugCorpus.setSummaryPart(rs.getString("SMR_COR"));
				bugCorpus.setDescriptionPart(rs.getString("DESC_COR"));
				bugCorpus.setContentNorm(rs.getDouble("COR_NORM"));
				bugCorpus.setSummaryCorpusNorm(rs.getDouble("SMR_COR_NORM"));
				bugCorpus.setDecriptionCorpusNorm(rs.getDouble("DESC_COR_NORM"));
				bug.setCorpus(bugCorpus);
				
				bug.setTotalCorpusCount(rs.getInt("TOT_CNT"));
				bug.setVersion(rs.getString("VER"));
				bugs.put(bug.getID(), bug);
			}
			
			Iterator<Integer> bugsIter = bugs.keySet().iterator();
			while (bugsIter.hasNext()) {
				int bugID = bugsIter.next();
				bug = bugs.get(bugID);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bugs;	
	}
	
	
	public ArrayList<Bug> getAllBugs(boolean orderedByFixedDate) {
		ArrayList<Bug> bugs = new ArrayList<Bug>();
		
		String sql = "SELECT BUG_ID, OPEN_DATE, FIXED_DATE, SMR_COR, DESC_COR, CMT_COR, TOT_CNT, COR_NORM, SMR_COR_NORM, DESC_COR_NORM, VER FROM BUG_INFO ";
		
		if (orderedByFixedDate) {
			sql += "ORDER BY FIXED_DATE";
		}
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			Bug bug = null;
			rs = ps.executeQuery();
			while (rs.next()) {
				bug = new Bug();
				bug.setID(rs.getInt("BUG_ID"));
				bug.setOpenDate(rs.getTimestamp("OPEN_DATE"));
				bug.setFixedDate(rs.getTimestamp("FIXED_DATE"));

				BugCorpus bugCorpus = new BugCorpus();
				bugCorpus.setSummaryPart(rs.getString("SMR_COR"));
				bugCorpus.setDescriptionPart(rs.getString("DESC_COR"));
				bugCorpus.setCommentPart(rs.getString("CMT_COR"));
				bugCorpus.setContentNorm(rs.getDouble("COR_NORM"));
				bugCorpus.setSummaryCorpusNorm(rs.getDouble("SMR_COR_NORM"));
				bugCorpus.setDecriptionCorpusNorm(rs.getDouble("DESC_COR_NORM"));
				bug.setCorpus(bugCorpus);

				bug.setTotalCorpusCount(rs.getInt("TOT_CNT"));
				bug.setVersion(rs.getString("VER"));
				bugs.add(bug);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bugs;	
	}
	
	public HashMap<Integer, Double> getAllNorms() {
		HashMap<Integer, Double> bugNormMap = new HashMap<Integer, Double>();
		
		String sql = "SELECT BUG_ID, COR_NORM FROM BUG_INFO ";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);

			rs = ps.executeQuery();
			while (rs.next()) {
				int bugID = rs.getInt("BUG_ID");
				double bugNorm = rs.getDouble("COR_NORM");
				bugNormMap.put(bugID, bugNorm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bugNormMap;	
	}

	public int getBugCountWithDate(String dateString) {
		String sql = "SELECT COUNT(BUG_ID) FROM BUG_INFO " +
				"WHERE FIXED_DATE <= ?";
		
		int count = 0;
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setString(1, dateString);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				count = rs.getInt(1); 
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;	
	}
	
	public ArrayList<Bug> getPreviousFixedBugs(String fixedDateString, int exceptedBugID) {
		ArrayList<Bug> bugs = new ArrayList<Bug>();
		
		String sql = "SELECT BUG_ID, OPEN_DATE, FIXED_DATE, SMR_COR, DESC_COR, TOT_CNT, COR_NORM, SMR_COR_NORM, DESC_COR_NORM, VER FROM BUG_INFO " +
				"WHERE FIXED_DATE <= ? AND BUG_ID != ? ORDER BY FIXED_DATE";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setString(1, fixedDateString);
			ps.setInt(2, exceptedBugID);
			
			Bug bug = null;
			rs = ps.executeQuery();
			while (rs.next()) {
				bug = new Bug();
				bug.setID(rs.getInt("BUG_ID"));
				bug.setOpenDate(rs.getTimestamp("OPEN_DATE"));
				bug.setFixedDate(rs.getTimestamp("FIXED_DATE"));

				BugCorpus bugCorpus = new BugCorpus();
				bugCorpus.setSummaryPart(rs.getString("SMR_COR"));
				bugCorpus.setDescriptionPart(rs.getString("DESC_COR"));
				bugCorpus.setContentNorm(rs.getDouble("COR_NORM"));
				bugCorpus.setSummaryCorpusNorm(rs.getDouble("SMR_COR_NORM"));
				bugCorpus.setDecriptionCorpusNorm(rs.getDouble("DESC_COR_NORM"));
				bug.setCorpus(bugCorpus);

				bug.setTotalCorpusCount(rs.getInt("TOT_CNT"));
				bug.setVersion(rs.getString("VER"));
				bugs.add(bug);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bugs;	
	}
	
	public Bug getBug(int bugID) {
		String sql = "SELECT OPEN_DATE, FIXED_DATE, SMR_COR, DESC_COR, TOT_CNT, VER FROM BUG_INFO WHERE BUG_ID = ?";
		Bug bug = null;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, bugID);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				bug = new Bug();
				bug.setID(bugID);
				bug.setOpenDate(rs.getTimestamp("OPEN_DATE"));
				bug.setFixedDate(rs.getTimestamp("FIXED_DATE"));

				BugCorpus bugCorpus = new BugCorpus();
				bugCorpus.setSummaryPart(rs.getString("SMR_COR"));
				bugCorpus.setDescriptionPart(rs.getString("DESC_COR"));
				bug.setCorpus(bugCorpus);

				bug.setTotalCorpusCount(rs.getInt("TOT_CNT"));
				bug.setVersion(rs.getString("VER"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return bug;	
	}

	
	public int insertBugTerm(String term) {
		String sql = "INSERT INTO BUG_TERM_INFO (TERM) VALUES (?)";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setString(1, term);
			
			returnValue = ps.executeUpdate();
			
			sql = "SELECT BUG_TERM_ID FROM BUG_TERM_INFO WHERE TERM = ?";
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setString(1, term);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				returnValue = rs.getInt("BUG_TERM_ID");	
			}
		} catch (JdbcSQLException e) {
			e.printStackTrace();
			
			if (ErrorCode.DUPLICATE_KEY_1 != e.getErrorCode()) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public int deleteAllTerms() {
		String sql = "DELETE FROM BUG_TERM_INFO";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	/**
	 * Get <Source file name, Corpus sets> with product name and version
	 * 
	 * @return HashMap<Integer, String>	<Source file name, Corpus sets>
	 */
	public HashMap<Integer, String> getCorpusMap() {
		HashMap<Integer, String> corpusMap = new HashMap<Integer, String>();
		
		String sql = "SELECT BUG_ID, COR " +
					"FROM BUG_INFO";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				corpusMap.put(rs.getInt("BUG_ID"), rs.getString("COR"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return corpusMap;
	}
	
	/**
	 * Get norm value with bug ID
	 * 
	 * @param bugID		Bug ID
	 * @return double 	corpus norm value
	 */
	public double getNormValue(int bugID) {
		double norm = 0;
		
		String sql = "SELECT COR_NORM " +
					"FROM BUG_INFO " +
					"WHERE BUG_ID = ?";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, bugID);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				norm = rs.getDouble("COR_NORM");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return norm;
	}
	
	public double getNormValueCL(int bugID) {
		double norm = 0;
		
		String sql = "SELECT COR_NORM_CLASSIC " +
					"FROM BUG_INFO " +
					"WHERE BUG_ID = ?";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, bugID);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				norm = rs.getDouble("COR_NORM_CLASSIC");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return norm;
	}

	
	public HashMap<String, Integer> getTermMap() {
		HashMap<String, Integer> termMap = new HashMap<String, Integer>();
		
		String sql = "SELECT TERM, BUG_TERM_ID FROM BUG_TERM_INFO";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				termMap.put(rs.getString("TERM"), rs.getInt("BUG_TERM_ID"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return termMap;
	}
	
	public int getAllTermCount() {
		String sql = "SELECT COUNT(BUG_TERM_ID) FROM BUG_TERM_INFO";
		
		int allTermCount = 0;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				allTermCount = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return allTermCount;
	}
	
	public int getBugCount() {
		String sql = "SELECT COUNT(BUG_ID) FROM BUG_INFO";
		
		int bugCount = 0;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				bugCount = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bugCount;
	}
	
	public int getSfTermID(String term) {
		int returnValue = INVALID;
		String sql = "SELECT MTH_TERM_ID FROM MTH_TERM_INFO WHERE TERM = ?";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setString(1, term);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				returnValue = rs.getInt("MTH_TERM_ID");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnValue;	
	}
	
	public HashMap<String, AnalysisValue> getMthTermMap(int bugID) {
		String sql = "SELECT A.TF, A.IDF, A.TF_CLASSIC, B.TERM FROM BUG_MTH_TERM_WGT A, MTH_TERM_INFO B WHERE A.BUG_ID = ? AND A.MTH_TERM_ID = B.MTH_TERM_ID";
		
		HashMap<String, AnalysisValue> methodTermMap = null;
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, bugID);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				if (null == methodTermMap) {
					methodTermMap = new HashMap<String, AnalysisValue>();
				}
				
				AnalysisValue sourceFileTermWeight = new AnalysisValue();
				sourceFileTermWeight.setTf(rs.getDouble("TF"));
				sourceFileTermWeight.setIdf(rs.getDouble("IDF"));
				sourceFileTermWeight.setTfClassic(rs.getDouble("TF_CLASSIC"));
				
				String term = rs.getString("TERM");
				methodTermMap.put(term, sourceFileTermWeight);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return methodTermMap;	
	}
	
 
	public int insertBugMthTermWeight(AnalysisValue bugMthTermWeight) {
		int termID = getSfTermID(bugMthTermWeight.getTerm());
		
		String sql = "INSERT INTO BUG_MTH_TERM_WGT (BUG_ID, MTH_TERM_ID, TERM_CNT, INV_DOC_CNT, TF, IDF, TF_CLASSIC) " +
				"VALUES (?, ?, ?, ?, ?, ?, ?)";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, bugMthTermWeight.getBugID());
			ps.setInt(2, termID);
			ps.setInt(3, bugMthTermWeight.getTermCount());
			ps.setInt(4, bugMthTermWeight.getInvDocCount());
			ps.setDouble(5, bugMthTermWeight.getTf());
			ps.setDouble(6, bugMthTermWeight.getIdf());
			ps.setDouble(7, bugMthTermWeight.getTfClassic());
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	
	public int deleteAllBugMthTermWeights() {
		String sql = "DELETE FROM BUG_MTH_TERM_WGT";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public AnalysisValue getBugSfTermWeight(int bugID, String term) {
		AnalysisValue termWeight = null;

		String sql = "SELECT C.TERM_CNT, C.INV_DOC_CNT, C.TF, C.IDF "+
				"FROM MTH_TERM_INFO B, BUG_MTH_TERM_WGT C " +
				"WHERE C.BUG_ID = ? AND " +
				"B.TERM = ? AND " +
				"B.MTH_TERM_ID = C.MTH_TERM_ID";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, bugID);
			ps.setString(2, term);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				termWeight = new AnalysisValue();
				
				termWeight.setBugID(bugID);
				termWeight.setTerm(term);
				termWeight.setTermCount(rs.getInt("TERM_CNT"));
				termWeight.setInvDocCount(rs.getInt("INV_DOC_CNT"));
				termWeight.setTf(rs.getDouble("TF"));
				termWeight.setIdf(rs.getDouble("IDF"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return termWeight;
	}
	
	public AnalysisValue getBugMthTermWeight(int bugID, String term) {
		AnalysisValue termWeight = null;

		String sql = "SELECT C.TERM_CNT, C.INV_DOC_CNT, C.TF, C.IDF "+
				"FROM MTH_TERM_INFO B, BUG_MTH_TERM_WGT C " +
				"WHERE C.BUG_ID = ? AND " +
				"B.TERM = ? AND " +
				"B.MTH_TERM_ID = C.MTH_TERM_ID";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, bugID);
			ps.setString(2, term);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				termWeight = new AnalysisValue();
				
				termWeight.setBugID(bugID);
				termWeight.setTerm(term);
				termWeight.setTermCount(rs.getInt("TERM_CNT"));
				termWeight.setInvDocCount(rs.getInt("INV_DOC_CNT"));
				termWeight.setTf(rs.getDouble("TF"));
				termWeight.setIdf(rs.getDouble("IDF"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return termWeight;
	}
	
	public int getBugTermID(String term) {
		int returnValue = INVALID;
		String sql = "SELECT BUG_TERM_ID FROM BUG_TERM_INFO WHERE TERM = ?";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setString(1, term);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				returnValue = rs.getInt("BUG_TERM_ID");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnValue;	
	}
	
	public int insertBugTermWeight(AnalysisValue analysisValue) {
		int termID = analysisValue.getTermID();
		if (INVALID == termID) {
			termID = getBugTermID(analysisValue.getTerm());
		}
		
		String sql = "INSERT INTO BUG_TERM_WGT (BUG_ID, BUG_TERM_ID, TW) " +
				"VALUES (?, ?, ?)";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, analysisValue.getBugID());
			ps.setInt(2, termID);
			ps.setDouble(3, analysisValue.getTermWeight());
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public int deleteAllBugTermWeights() {
		String sql = "DELETE FROM BUG_TERM_WGT";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public AnalysisValue getBugTermWeight(int bugID, String term) {
		AnalysisValue returnValue = null;

		String sql = "SELECT C.TW "+
				"FROM BUG_INFO A, BUG_TERM_INFO B, BUG_TERM_WGT C " +
				"WHERE A.BUG_ID = ? AND "+
				"A.BUG_ID = C.BUG_ID AND B.TERM = ? AND B.BUG_TERM_ID = C.BUG_TERM_ID";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, bugID);
			ps.setString(2, term);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				returnValue = new AnalysisValue();
				
				returnValue.setBugID(bugID);
				returnValue.setTerm(term);
				returnValue.setTermWeight(rs.getDouble("TW"));				
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public ArrayList<AnalysisValue> getBugTermWeightList(int bugID) {
		ArrayList<AnalysisValue> bugAnalysisValues = null;
		AnalysisValue bugTermWeight = null;

		String sql = "SELECT B.TERM, C.BUG_TERM_ID, C.TW "+
				"FROM BUG_TERM_INFO B, BUG_TERM_WGT C " +
				"WHERE C.BUG_ID = ? AND B.BUG_TERM_ID = C.BUG_TERM_ID ORDER BY C.BUG_TERM_ID";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, bugID);
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				if (null == bugAnalysisValues) {
					bugAnalysisValues = new ArrayList<AnalysisValue>();
				}
				
				bugTermWeight = new AnalysisValue();
				bugTermWeight.setBugID(bugID);
				bugTermWeight.setTerm(rs.getString("TERM"));
				bugTermWeight.setTermID(rs.getInt("BUG_TERM_ID"));
				bugTermWeight.setTermWeight(rs.getDouble("TW"));
				
				bugAnalysisValues.add(bugTermWeight);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return bugAnalysisValues;
	}
	
public int insertBugFixedMethodInfo(int bugID, Method method) {
		String sql = "INSERT INTO BUG_FIX_MTH_INFO (BUG_ID, FIXED_MTH_ID) VALUES (?, ?)";
		int returnValue = INVALID;
		
		try {
			MethodDAO methodDAO = new MethodDAO();
			int fixedMethodID = methodDAO.getMethodID(method);;
			
			if (INVALID != fixedMethodID) {
				ps = analysisDbConnection.prepareStatement(sql);
				ps.setInt(1, bugID);
				ps.setInt(2, fixedMethodID);
				
				returnValue = ps.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}

	
	public int deleteAllBugFixedInfo() {
		String sql = "DELETE FROM BUG_FIX_MTH_INFO";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		sql = "DELETE FROM BUG_FIX_MTH_INFO";
		returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	

	public HashSet<Method> getFixedMethods(int bugID) {
		HashSet<Method> fixedMethods = null;
		
		String sql = "SELECT A.MTH_ID, A.SF_NAME, A.MTH_NAME, A.RET_TYPE, A.PARAMS, A.HASH_KEY FROM MTH_INFO A, BUG_FIX_MTH_INFO B " + 
				"WHERE B.BUG_ID = ? AND B.FIXED_MTH_ID = A.MTH_ID";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, bugID);
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				if (null == fixedMethods) {
					fixedMethods = new HashSet<Method>();
				}

				Method method = new Method();
				method.setID(rs.getInt("MTH_ID"));
				method.setSourceFileName(rs.getString("SF_NAME"));
				method.setName(rs.getString("MTH_NAME"));
				method.setReturnType(rs.getString("RET_TYPE"));
				method.setParams(rs.getString("PARAMS"));
				method.setHashKey(rs.getString("HASH_KEY"));

				fixedMethods.add(method);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return fixedMethods;
	}
	
	public int insertSimilarBugInfo(int bugID, int similarBugID, double similarityScore) {
		String sql = "INSERT INTO SIMI_BUG_ANAYSIS (BUG_ID, SIMI_BUG_ID, SIMI_BUG_SCORE) VALUES (?, ?, ?)";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, bugID);
			ps.setInt(2, similarBugID);
			ps.setDouble(3, similarityScore);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public int deleteAllSimilarBugInfo() {
		String sql = "DELETE FROM SIMI_BUG_ANAYSIS";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public HashSet<SimilarBugInfo> getSimilarBugInfos(int bugID) {
		HashSet<SimilarBugInfo> similarBugInfos = null;

		String sql = "SELECT SIMI_BUG_ID, SIMI_BUG_SCORE FROM SIMI_BUG_ANAYSIS " + 
				"WHERE BUG_ID = ? AND SIMI_BUG_SCORE != 0.0";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, bugID);
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				if (null == similarBugInfos) {
					similarBugInfos = new HashSet<SimilarBugInfo>();
				}

				SimilarBugInfo similarBugInfo = new SimilarBugInfo();
				similarBugInfo.setSimilarBugID(rs.getInt("SIMI_BUG_ID"));
				similarBugInfo.setSimilarityScore(rs.getDouble("SIMI_BUG_SCORE"));

				similarBugInfos.add(similarBugInfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return similarBugInfos;
	}
	
	public int updateTotalTermCount(int bugID, int totalTermCount) {
		String sql = "UPDATE BUG_INFO SET TOT_CNT = ? " +
				"WHERE BUG_ID = ?";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, totalTermCount);
			ps.setInt(2, bugID);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	
	
	public int updateNormValues(int bugID, double corpusNorm, double corpusNormCL, double summaryCorpusNorm, double descriptionCorpusNorm) {
		String sql = "UPDATE BUG_INFO SET COR_NORM = ?, COR_NORM_CLASSIC = ?, SMR_COR_NORM = ?, DESC_COR_NORM = ? " +
				"WHERE BUG_ID = ?";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setDouble(1, corpusNorm);
			ps.setDouble(2, corpusNormCL);
			ps.setDouble(3, summaryCorpusNorm);
			ps.setDouble(4, descriptionCorpusNorm);
			
			ps.setInt(5, bugID);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	
	
	public int insertComment(int bugID, Comment comment) {
		String sql = "INSERT INTO BUG_CMT_INFO (BUG_ID, CMT_ID, ATHR, CMT_DATE, CMT_COR) VALUES (?, ?, ?, ?, ?)";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, bugID);
			ps.setInt(2, comment.getID());
			ps.setString(3, comment.getAuthor());
			ps.setString(4, comment.getCommentedDateString());
			ps.setString(5, comment.getCommentCorpus());
			
			returnValue = ps.executeUpdate();
		} catch (JdbcSQLException e) {
			e.printStackTrace();
			
			if (ErrorCode.DUPLICATE_KEY_1 != e.getErrorCode()) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public int deleteAllComments() {
		String sql = "DELETE FROM BUG_CMT_INFO";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}

	public ArrayList<Comment> getComments(int bugID) {
		ArrayList<Comment> comments = null;

		String sql = "SELECT CMT_ID, ATHR, CMT_DATE, CMT_COR "+
				"FROM BUG_CMT_INFO " +
				"WHERE BUG_ID = ?";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, bugID);
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				if (null == comments) {
					comments = new ArrayList<Comment>();
				}
				
				Comment comment = new Comment(rs.getInt("CMT_ID"), rs.getTimestamp("CMT_DATE"),
						rs.getString("ATHR"), rs.getString("CMT_COR"));
				comments.add(comment);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return comments;
	}
}
