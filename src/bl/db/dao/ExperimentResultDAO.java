package bl.db.dao;

import bl.db.ExperimentResult;

/**
 * @author Abdul
 *
 */
public class ExperimentResultDAO extends BaseDAO {

	/**
	 * @throws Exception
	 */
	public ExperimentResultDAO() throws Exception {
		super();
	}
	
	public int insertExperimentResult(ExperimentResult experimentResult) {
		String sql = "INSERT INTO EXP_INFO (TOP1, TOP5, TOP10, TOP1_RATE, TOP5_RATE, TOP10_RATE, MRR, MAP, PROD_NAME, ALPHA,  PAST_DAYS, EXP_DATE) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		int returnValue = INVALID;
		
		try {
			ps = evaluationDbConnection.prepareStatement(sql);
			ps.setInt(1, experimentResult.getTop1());
			ps.setInt(2, experimentResult.getTop5());
			ps.setInt(3, experimentResult.getTop10());
			ps.setDouble(4, experimentResult.getTop1Rate());
			ps.setDouble(5, experimentResult.getTop5Rate());
			ps.setDouble(6, experimentResult.getTop10Rate());
			ps.setDouble(7, experimentResult.getMRR());
			ps.setDouble(8, experimentResult.getMAP());
			ps.setString(9, experimentResult.getProductName());
			ps.setDouble(10, experimentResult.getAlpha());
			ps.setInt(11, experimentResult.getPastDays());
			ps.setString(12, experimentResult.getExperimentDateString());
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}

	public int deleteAllExperimentResults() {
		String sql = "DELETE FROM EXP_INFO";
		int returnValue = INVALID;
		
		try {
			ps = evaluationDbConnection.prepareStatement(sql);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public ExperimentResult getExperimentResult(String productName) {
		ExperimentResult returnValue = null;

		String sql = "SELECT TOP1, TOP5, TOP10, TOP1_RATE, TOP5_RATE, TOP10_RATE, MRR, MAP,  ALPHA, PAST_DAYS, EXP_DATE "+
				"FROM EXP_INFO " +
				"WHERE PROD_NAME = ? ";
		
		try {
			ps = evaluationDbConnection.prepareStatement(sql);
			ps.setString(1, productName);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				returnValue = new ExperimentResult();
				
				returnValue.setTop1(rs.getInt("TOP1"));
				returnValue.setTop5(rs.getInt("TOP5"));
				returnValue.setTop10(rs.getInt("TOP10"));
				returnValue.setTop1Rate(rs.getDouble("TOP1_RATE"));
				returnValue.setTop5Rate(rs.getDouble("TOP5_RATE"));
				returnValue.setTop10Rate(rs.getDouble("TOP10_RATE"));
				returnValue.setMRR(rs.getDouble("MRR"));
				returnValue.setMAP(rs.getDouble("MAP"));
				returnValue.setProductName(productName);

				returnValue.setAlpha(rs.getDouble("ALPHA"));

				returnValue.setPastDays(rs.getInt("PAST_DAYS"));
				returnValue.setExperimentDate(rs.getTimestamp("EXP_DATE"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
}
