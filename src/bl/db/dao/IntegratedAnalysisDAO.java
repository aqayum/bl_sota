package bl.db.dao;

import java.util.ArrayList;
import java.util.HashMap;

import bl.db.ExtendedIntegratedAnalysisValue;
import bl.db.IntegratedAnalysisValue;

/**
 * @author Abdul
 *
 */
public class IntegratedAnalysisDAO extends BaseDAO {
	
	public final static int INVALID_SCORE = -1;

	/**
	 * @throws Exception
	 */
	public IntegratedAnalysisDAO() throws Exception {
		super();
	}
	
	public int insertAnalysisVaule(IntegratedAnalysisValue integratedAnalysisValue) {
		String sql = "INSERT INTO INT_ANALYSIS (BUG_ID, MTH_ID, VSM_SCORE, SIMI_SCORE, BL_SCORE,  COMM_SCORE, VSM_CLASSIC_SCORE ) "+
				"VALUES (?, ?, ?, ?, ?, ?, ?)";
		int returnValue = INVALID;
		
		try {
			int methodID = integratedAnalysisValue.getMethodID();
			
			System.out.println("bug id "+integratedAnalysisValue.getBugID()+" method id "+ methodID);
			if(methodID==2066)
				System.out.println();
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, integratedAnalysisValue.getBugID());
			ps.setInt(2, methodID);
			ps.setDouble(3, integratedAnalysisValue.getVsmScore());
			ps.setDouble(4, integratedAnalysisValue.getSimilarityScore());
			ps.setDouble(5, integratedAnalysisValue.getBugLocatorScore());
			ps.setDouble(6, integratedAnalysisValue.getCommitLogScore());
			ps.setDouble(7, integratedAnalysisValue.getVsmClassicScore());
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	
	
	public int updateVsmScore(IntegratedAnalysisValue integratedAnalysisValue) {
		String sql = "UPDATE INT_ANALYSIS SET VSM_SCORE = ? WHERE BUG_ID = ? AND MTH_ID = ?";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setDouble(1, integratedAnalysisValue.getVsmScore());
			ps.setInt(2, integratedAnalysisValue.getBugID());
			ps.setInt(3, integratedAnalysisValue.getSourceMethodVersionID());
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	
	
	public int updateSimilarScore(IntegratedAnalysisValue integratedAnalysisValue) {
		String sql = "UPDATE INT_ANALYSIS SET SIMI_SCORE = ? WHERE BUG_ID = ? AND MTH_ID = ?";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setDouble(1, integratedAnalysisValue.getSimilarityScore());
			ps.setInt(2, integratedAnalysisValue.getBugID());
			ps.setInt(3, integratedAnalysisValue.getSourceMethodVersionID());
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	public int updateCommitScore(IntegratedAnalysisValue integratedAnalysisValue) {
		String sql = "UPDATE INT_ANALYSIS SET COMM_SCORE = ? WHERE BUG_ID = ? AND MTH_ID = ?";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setDouble(1, integratedAnalysisValue.getCommitLogScore());
			ps.setInt(2, integratedAnalysisValue.getBugID());
			ps.setInt(3, integratedAnalysisValue.getSourceMethodVersionID());
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	public int updateBugLocatorScore(IntegratedAnalysisValue integratedAnalysisValue) {
		String sql = "UPDATE INT_ANALYSIS SET BL_SCORE = ? WHERE BUG_ID = ? AND MTH_ID = ?";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setDouble(1, integratedAnalysisValue.getBugLocatorScore());
			ps.setInt(2, integratedAnalysisValue.getBugID());
			ps.setInt(3, integratedAnalysisValue.getSourceMethodVersionID());
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;		
	}
	
	
	public int updateCommitLogScore(int methodID, IntegratedAnalysisValue integratedAnalysisValue) throws Exception {
		String sql = "UPDATE INT_ANALYSIS SET COMM_SCORE = ? WHERE BUG_ID = ? AND MTH_ID = ?";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setDouble(1, integratedAnalysisValue.getCommitLogScore());
			ps.setInt(2, integratedAnalysisValue.getBugID());
			ps.setInt(3, methodID);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;		
	}
	

	public int deleteAllIntegratedAnalysisInfos() {
		String sql = "DELETE FROM INT_ANALYSIS";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		

			return returnValue;
		
	}
	
	
	public HashMap<Integer, IntegratedAnalysisValue> getAnalysisValues(int bugID) {
		HashMap<Integer, IntegratedAnalysisValue> integratedAnalysisValues = null;
		IntegratedAnalysisValue resultValue = null;

		String sql = "SELECT A.MTH_ID, A.VSM_SCORE, A.SIMI_SCORE, A.BL_SCORE, A.COMM_SCORE  "+
				"FROM INT_ANALYSIS A " +
				"WHERE A.BUG_ID = ?";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, bugID);
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				if (null == integratedAnalysisValues) {
					integratedAnalysisValues = new HashMap<Integer, IntegratedAnalysisValue>();
				}
				
				resultValue = new IntegratedAnalysisValue();
				resultValue.setBugID(bugID);
				resultValue.setMethodID(rs.getInt("MTH_ID"));
				resultValue.setVsmScore(rs.getDouble("VSM_SCORE"));
				resultValue.setSimilarityScore(rs.getDouble("SIMI_SCORE"));
				resultValue.setBugLocatorScore(rs.getDouble("BL_SCORE"));
				resultValue.setCommitLogScore(rs.getDouble("COMM_SCORE"));

				
				integratedAnalysisValues.put(resultValue.getSourceMethodVersionID(), resultValue);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return integratedAnalysisValues;
	}
	
	public ArrayList<IntegratedAnalysisValue> getBugLocatorRankedValues(int bugID, int limit) {
		ArrayList<IntegratedAnalysisValue> bugLocatorRankedValues = null;
		ExtendedIntegratedAnalysisValue resultValue = null;

		String sql = "SELECT C.MTH_NAME, A.MTH_ID, A.VSM_SCORE, A.SIMI_SCORE, A.BL_SCORE "+
				"FROM INT_ANALYSIS A, MTH_INFO C " +
				"WHERE A.BUG_ID = ? AND A.MTH_ID = C.MTH_ID AND A.BL_SCORE != 0" +
				"ORDER BY A.BL_SCORE DESC ";
		
		if (limit != 0) {
			sql += "LIMIT " + limit;
		}
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, bugID);
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				if (null == bugLocatorRankedValues) {
					bugLocatorRankedValues = new ArrayList<IntegratedAnalysisValue>();
				}
				
				resultValue = new ExtendedIntegratedAnalysisValue();
				resultValue.setBugID(bugID);
				resultValue.setMethodName(rs.getString("MTH_NAME"));
				resultValue.setMethodID(rs.getInt("MTH_ID"));
				resultValue.setVsmScore(rs.getDouble("VSM_SCORE"));
				resultValue.setSimilarityScore(rs.getDouble("SIMI_SCORE"));
				resultValue.setBugLocatorScore(rs.getDouble("BL_SCORE"));

				
				bugLocatorRankedValues.add(resultValue);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return bugLocatorRankedValues;
	}
	
	
	
	
}
