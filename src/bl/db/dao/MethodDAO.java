package bl.db.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.h2.api.ErrorCode;
import org.h2.jdbc.JdbcSQLException;

import bl.common.Method;
import bl.db.AnalysisValue;

/**
 * @author Abdul
 *
 */
public class MethodDAO extends BaseDAO {

	final static public String DEFAULT_VERSION_STRING = "v1.0";
	final static public double INIT_LENGTH_SCORE = 0.0;
	/**
	 * @throws Exception
	 */
	public MethodDAO() throws Exception {
		super();
	}
	
	public int insertMethod(Method method, String version) {
		String sql = "INSERT INTO MTH_INFO (SF_NAME, MTH_NAME, RET_TYPE, PARAMS, MTH_CORPUS, VERSION, HASH_KEY) VALUES (?, ?, ?, ?, ?,?,?)";
		int returnValue = INVALID;
		//System.out.println("inserting method");
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setString(1, method.getSourceFileName());
			ps.setString(2, method.getName());
			ps.setString(3, method.getReturnType());
			ps.setString(4, method.getParams());
			ps.setString(5, method.getCorpus());
			//System.out.println(method.getSourceFileName()+ " : "+method.getName() + " : "+method.getParams());
			ps.setString(6, version);
			ps.setString(7, method.getHashKey());
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (INVALID != returnValue) {
			returnValue = getMethodID(method);
		} 

		return returnValue;
	}
	public ArrayList< Integer> getMethodIDs() {
		ArrayList< Integer> methodIDs = new ArrayList< Integer>();
		
		String sql = "SELECT MTH_ID " +
					"FROM MTH_INFO ";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				methodIDs.add(rs.getInt("MTH_ID"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return methodIDs;
	}
	
	
	public int insertMethod(Method method) {
		String sql = "INSERT INTO MTH_INFO (SF_NAME, MTH_NAME, RET_TYPE, PARAMS,MTH_CORPUS, HASH_KEY) VALUES (?, ?, ?, ?, ?,?)";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setString(1, method.getSourceFileName());
			ps.setString(2, method.getName());
			ps.setString(3, method.getReturnType());
			ps.setString(4, method.getParams());
			ps.setString(5, method.getCorpus());
			ps.setString(6, method.getHashKey());
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (INVALID != returnValue) {
			returnValue = getMethodID(method);
		} 

		return returnValue;
	}
	
	public int getMethodID(Method method) {
		int returnValue = INVALID;
		String sql = "SELECT MTH_ID FROM MTH_INFO " +
				"WHERE HASH_KEY = ? AND MTH_NAME = ?";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setString(1, method.getHashKey());
			ps.setString(2, method.getName());
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				returnValue = rs.getInt("MTH_ID");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnValue;	
	}
	public double getLengthScore(int methodID) {
		double lengthScore = INIT_LENGTH_SCORE;

		String sql = "SELECT LEN_SCORE " +
					"FROM MTH_INFO " +
					"WHERE MTH_ID = ?";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, methodID);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				lengthScore = rs.getDouble("LEN_SCORE");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lengthScore;
	}
	// <Hash key, Method>
	public HashMap<String, Method> getMethods(int methodID) {
		HashMap<String, Method> methodInfo = new HashMap<String, Method>();
		
		String sql = "SELECT MTH_ID, SF_NAME, MTH_NAME, RET_TYPE, PARAMS, MTH_CORPUS, HASH_KEY FROM MTH_INFO WHERE MTH_ID = ?";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, methodID);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				Method method = new Method(rs.getInt("MTH_ID"), rs.getString("SF_NAME"),
						rs.getString("MTH_NAME"), rs.getString("RET_TYPE"), rs.getString("PARAMS"), rs.getString("MTH_CORPUS"),
						rs.getString("HASH_KEY"));
				methodInfo.put(rs.getString("HASH_KEY"), method);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return methodInfo;
	}
	
	public Method getMethod(int methodID) {
		Method methodInfo = new Method();
		
		String sql = "SELECT MTH_ID, SF_NAME, MTH_NAME, RET_TYPE, PARAMS, MTH_CORPUS, HASH_KEY FROM MTH_INFO WHERE MTH_ID = ?";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, methodID);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				methodInfo = new Method(rs.getInt("MTH_ID"), rs.getString("SF_NAME"),
						rs.getString("MTH_NAME"), rs.getString("RET_TYPE"), rs.getString("PARAMS"), rs.getString("MTH_CORPUS"),
						rs.getString("HASH_KEY"));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return methodInfo;
	}
	// <Hash key, Method>
	public ArrayList<Method> getAllMethods() {
		ArrayList<Method> methodMap = new ArrayList<Method>();
		
		String sql = "SELECT MTH_ID, SF_NAME, MTH_NAME, RET_TYPE, PARAMS, MTH_CORPUS, HASH_KEY FROM MTH_INFO";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				int methodID = rs.getInt("MTH_ID");
				Method method = new Method(methodID, rs.getString("SF_NAME"),
						rs.getString("MTH_NAME"), rs.getString("RET_TYPE"), rs.getString("PARAMS"), rs.getString("MTH_CORPUS"),
						rs.getString("HASH_KEY"));
				if (method != null) {
					methodMap.add(method);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return methodMap;
	}
	public double getCopusNorm(int methodID) {
		double corpusNorm = INIT_LENGTH_SCORE;

		String sql = "SELECT COR_NORM " +
					"FROM MTH_INFO " +
					"WHERE MTH_ID = ?";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, methodID);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				corpusNorm = rs.getDouble("COR_NORM");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return corpusNorm;
	}
	
	public double getCopusNormCL(int methodID) {
		double corpusNorm = INIT_LENGTH_SCORE;

		String sql = "SELECT COR_NORM_CLASSIC " +
					"FROM MTH_INFO " +
					"WHERE MTH_ID = ?";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, methodID);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				corpusNorm = rs.getDouble("COR_NORM_CLASSIC");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return corpusNorm;
	}
	
	public int updateNormValues(int methodID, double corpusNorm, double corpusNormCL) {
		String sql = "UPDATE MTH_INFO SET COR_NORM = ?, COR_NORM_CLASSIC = ?" +
				"WHERE MTH_ID = ?";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setDouble(1, corpusNorm);
			ps.setDouble(2, corpusNormCL);
			ps.setInt(3, methodID);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	public int updateTermWeight(AnalysisValue termWeight) {
		String sql = "UPDATE MTH_TERM_WGT SET TERM_CNT = ?, INV_DOC_CNT = ?, TF = ?, IDF = ?, TF_CLASSIC=?" +
				"WHERE MTH_TERM_ID = ?";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, termWeight.getTermCount());
			ps.setInt(2, termWeight.getInvDocCount());
			ps.setDouble(3, termWeight.getTf());
			ps.setDouble(4, termWeight.getIdf());
			ps.setDouble(5, termWeight.getTfClassic());
			ps.setInt(6, termWeight.getTermID());
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public HashMap<Integer, Method> getAllMethodsMap() {
		HashMap<Integer, Method> methodMap = new HashMap<Integer, Method>();
		
		String sql = "SELECT MTH_ID, SF_NAME, MTH_NAME, RET_TYPE, PARAMS, MTH_CORPUS, HASH_KEY FROM MTH_INFO";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				int methodID = rs.getInt("MTH_ID");
				Method method = new Method(methodID, rs.getString("SF_NAME"),
						rs.getString("MTH_NAME"), rs.getString("RET_TYPE"), rs.getString("PARAMS"), rs.getString("MTH_CORPUS"),
						rs.getString("HASH_KEY"));
				if (method != null) {
					methodMap.put(methodID, method);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return methodMap;
	}
	
	public int insertTerm(String term) {
		String sql = "INSERT INTO MTH_TERM_INFO (TERM) VALUES (?)";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setString(1, term);
			
			returnValue = ps.executeUpdate();
		} catch (JdbcSQLException e) {
			e.printStackTrace();
			
			if (ErrorCode.DUPLICATE_KEY_1 != e.getErrorCode()) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}

	public int getTermID(String term) {
		int returnValue = INVALID;
		String sql = "SELECT MTH_TERM_ID FROM MTH_TERM_INFO WHERE TERM = ?";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setString(1, term);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				returnValue = rs.getInt("MTH_TERM_ID");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnValue;	
	}
	
	public int updateLengthScore(int methodID, double lengthScore, int corpusCount) {
		String sql = "UPDATE MTH_INFO SET LEN_SCORE = ?, CRP_COUNT=? " +
				"WHERE MTH_ID = ?";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setDouble(1, lengthScore);
			ps.setInt(2, corpusCount);
			ps.setInt(3, methodID);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	public int getSourceMethodCount() {
		String sql = "SELECT COUNT(MTH_ID) FROM MTH_INFO A";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				returnValue = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnValue;
	}
	
	public HashMap<Integer, Integer> getTotalCorpusLengths() {
		HashMap<Integer, Integer> totalCorpusLengths = new HashMap<Integer, Integer>();
		
		String sql = "SELECT MTH_ID, CRP_COUNT " +
					"FROM MTH_INFO ";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				totalCorpusLengths.put(rs.getInt("MTH_ID"), rs.getInt("CRP_COUNT"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return totalCorpusLengths;
	}
	
	public HashMap<String, Integer> getTermMap() {
		HashMap<String, Integer> methodInfo = new HashMap<String, Integer>();
		
		String sql = "SELECT TERM, MTH_TERM_ID FROM MTH_TERM_INFO";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				methodInfo.put(rs.getString("TERM"), rs.getInt("MTH_TERM_ID"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return methodInfo;
	}
	public HashMap<String, AnalysisValue> getTermMap(int methodID) {
		HashMap<String, AnalysisValue> termMap = null;

		String sql = "SELECT C.TERM, D.MTH_ID, D.MTH_TERM_ID, D.TERM_CNT, D.INV_DOC_CNT, D.TF, D.IDF,  D.TF_CLASSIC "+
				"FROM MTH_INFO A, MTH_TERM_INFO C, MTH_TERM_WGT D " +
				"WHERE A.MTH_ID = ? AND " +
				"A.MTH_ID = D.MTH_ID AND " +
				"C.MTH_TERM_ID = D.MTH_TERM_ID";
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, methodID);
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				if (null == termMap) {
					termMap = new HashMap<String, AnalysisValue>();
				}
				AnalysisValue termWeight = new AnalysisValue();
				
				String term = rs.getString("TERM");
				termWeight.setTerm(term);
				termWeight.setMethodID(rs.getInt("MTH_ID"));
				termWeight.setTermID(rs.getInt("MTH_TERM_ID"));
				termWeight.setTermCount(rs.getInt("TERM_CNT"));
				termWeight.setInvDocCount(rs.getInt("INV_DOC_CNT"));
				termWeight.setTf(rs.getDouble("TF"));
				termWeight.setIdf(rs.getDouble("IDF"));
				termWeight.setTfClassic(rs.getDouble("TF_CLASSIC"));
				
				termMap.put(term, termWeight);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return termMap;
	}
	public int insertTermWeight(AnalysisValue termWeight) {
		
		int termID = getTermID(termWeight.getTerm());
		
		String sql = "INSERT INTO MTH_TERM_WGT (MTH_ID, MTH_TERM_ID, VERSION, TERM_CNT, INV_DOC_CNT, TF, IDF) " +
				"VALUES (?, ?, ?, ?, ?, ?, ?)";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			ps.setInt(1, termWeight.getMethodID());
			ps.setInt(2, termID);
			ps.setString(3, termWeight.getVersion());
			ps.setInt(4, termWeight.getTermCount());
			ps.setInt(5, termWeight.getInvDocCount());
			ps.setDouble(6, termWeight.getTf());
			ps.setDouble(7, termWeight.getIdf());
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	

	
	public int deleteAllMethods() {
		String sql = "DELETE FROM MTH_INFO";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public int deleteAllTerms() {
		String sql = "DELETE FROM MTH_TERM_INFO";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public int deleteAllTermWeights() {
		String sql = "DELETE FROM MTH_TERM_WGT";
		int returnValue = INVALID;
		
		try {
			ps = analysisDbConnection.prepareStatement(sql);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
}
