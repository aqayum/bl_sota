package bl.db.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;


/**
 * @author Abdul
 *
 */
public class DbUtil {
	protected PreparedStatement ps = null;
	protected ResultSet rs = null;
	
	public void openConnetion() throws Exception {
		String dbName = BaseDAO.DEFAULT_DB_NAME;
		BaseDAO.openConnection(dbName);
	}
	
	public void openConnetion(String dbName) throws Exception {
		BaseDAO.openConnection(dbName);
	}
	
	public void openEvaluationDbConnection() throws Exception {
		BaseDAO.openEvaluationDbConnection();
	}
	
	public void closeConnection() throws Exception {
		BaseDAO.closeConnection();
	}
	
	public int createAllAnalysisTables() throws Exception {
		String sql = "CREATE MEMORY TABLE MTH_TERM_INFO (MTH_TERM_ID INT PRIMARY KEY HASH AUTO_INCREMENT, TERM VARCHAR(255)); " + 
				"CREATE UNIQUE HASH INDEX IDX_MTH_TERM ON MTH_TERM_INFO(TERM); " +
				
				"CREATE MEMORY TABLE MTH_TERM_WGT (MTH_ID INT, MTH_TERM_ID INT, VERSION VARCHAR(127), TERM_CNT INT, INV_DOC_CNT INT, TF DOUBLE, TF_CLASSIC DOUBLE, IDF DOUBLE); " +
				"CREATE UNIQUE INDEX COMP_IDX_MTH_TERM_WGT ON MTH_TERM_WGT(MTH_ID,MTH_TERM_ID); " +
				
				"CREATE MEMORY TABLE MTH_INFO(MTH_ID INT PRIMARY KEY HASH AUTO_INCREMENT, SF_NAME VARCHAR(255), VERSION VARCHAR(255), MTH_NAME VARCHAR(127), RET_TYPE VARCHAR(63), PARAMS VARCHAR(255), MTH_CORPUS VARCHAR, HASH_KEY VARCHAR(40), COR_NORM DOUBLE, COR_NORM_CLASSIC DOUBLE, CRP_COUNT INT, LEN_SCORE DOUBLE); " +
				"CREATE HASH INDEX IDX_MTH_INFO_MTH_ID ON MTH_INFO(MTH_ID); " +
				"CREATE HASH INDEX IDX_MTH_INFO_NAME ON MTH_INFO(MTH_NAME); " +
				"CREATE HASH INDEX IDX_MTH_INFO_HASH_KEY ON MTH_INFO(HASH_KEY); " +
				
				"CREATE MEMORY TABLE BUG_INFO(BUG_ID INT PRIMARY KEY HASH, OPEN_DATE DATETIME, FIXED_DATE DATETIME,"
				+ " SMR_COR VARCHAR, DESC_COR VARCHAR, CMT_COR VARCHAR, TOT_CNT INT,"
				+ " COR_NORM DOUBLE, SMR_COR_NORM DOUBLE, COR_NORM_CLASSIC DOUBLE, DESC_COR_NORM DOUBLE, VER VARCHAR(15)); " + 
				"CREATE INDEX IDX_BUG_INFO_FDATE ON BUG_INFO(FIXED_DATE); " +
				
				"CREATE MEMORY TABLE BUG_CMT_INFO(BUG_ID INT, CMT_ID INT, ATHR VARCHAR(31), CMT_DATE DATETIME, CMT_COR VARCHAR); " +
				"CREATE INDEX IDX_BUG_CMT_INFO ON BUG_CMT_INFO(BUG_ID); " +

				"CREATE MEMORY TABLE BUG_TERM_INFO(BUG_TERM_ID INT PRIMARY KEY HASH AUTO_INCREMENT, TERM VARCHAR(255)); " +
				"CREATE UNIQUE HASH INDEX IDX_BUG_TERM ON BUG_TERM_INFO(TERM); " +
				
				
				"CREATE MEMORY TABLE BUG_MTH_TERM_WGT(BUG_ID INT, MTH_TERM_ID INT, TERM_CNT INT, INV_DOC_CNT INT, TF DOUBLE, TF_CLASSIC DOUBLE, IDF DOUBLE); " +
				"CREATE UNIQUE INDEX COMP_IDX_BUG_MTH_TERM_WGT ON BUG_MTH_TERM_WGT(BUG_ID, MTH_TERM_ID); " +

					
				"CREATE MEMORY TABLE BUG_TERM_WGT(BUG_ID INT, BUG_TERM_ID INT, TW DOUBLE); " +
				"CREATE UNIQUE INDEX COMP_IDX_BUG_TERM_WGT ON BUG_TERM_WGT(BUG_ID, BUG_TERM_ID); " +
				
				"CREATE MEMORY TABLE BUG_FIX_MTH_INFO(BUG_ID INT, FIXED_MTH_ID INT); " +
				"CREATE INDEX IDX_BUG_FIX_MTH_INFO ON BUG_FIX_MTH_INFO(BUG_ID); " +

				
				"CREATE MEMORY TABLE SIMI_BUG_ANAYSIS(BUG_ID INT, SIMI_BUG_ID INT, SIMI_BUG_SCORE DOUBLE); " +
				"CREATE INDEX IDX_SIMI_BUG_ANAYSIS ON SIMI_BUG_ANAYSIS(BUG_ID); " +
				

				"CREATE MEMORY TABLE COMM_INFO(COMM_ID VARCHAR(127) PRIMARY KEY HASH, COMM_DATE DATETIME, MSG VARCHAR, COMMITTER VARCHAR(63)); " +
				
		
				"CREATE MEMORY TABLE COMM_MTH_INFO(COMM_ID VARCHAR(127), COMM_SF VARCHAR(255), COMM_MTH VARCHAR(255), COM_MTH_HASH_KEY VARCHAR(40)); " +
				"CREATE INDEX IDX_COMM_MTH_INFO ON COMM_MTH_INFO(COMM_ID); " +


				"CREATE MEMORY TABLE INT_ANALYSIS(BUG_ID INT, MTH_ID INT, VSM_SCORE DOUBLE, VSM_CLASSIC_SCORE DOUBLE, SIMI_SCORE DOUBLE, BL_SCORE DOUBLE,  COMM_SCORE DOUBLE); " +
				"CREATE INDEX IDX_INT_ANAL_BL_SCORE ON INT_ANALYSIS(BL_SCORE DESC); " +
				"CREATE INDEX IDX_INT_ANAL_COMM_SCORE ON INT_ANALYSIS(COMM_SCORE DESC); " +
				"CREATE INDEX IDX_INT_ANAL_BUG_ID ON INT_ANALYSIS(BUG_ID); " +
				"CREATE UNIQUE INDEX COMP_IDX_INT_ANALYSIS ON INT_ANALYSIS(BUG_ID, MTH_ID); " ;


		int returnValue = BaseDAO.INVALID;
		try {
			ps = BaseDAO.getAnalysisDbConnection().prepareStatement(sql);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public int createEvaluationTable() throws Exception {
		String sql = "CREATE MEMORY TABLE EXP_INFO(TOP1 INT, TOP5 INT, TOP10 INT, TOP1_RATE DOUBLE, TOP5_RATE DOUBLE, TOP10_RATE DOUBLE, MAP DOUBLE, MRR DOUBLE, PROD_NAME VARCHAR(31),"
					+ " ALG_NAME VARCHAR(31), ALG_DESC VARCHAR(255), ALPHA DOUBLE, BETA DOUBLE, GAMMA DOUBLE, PAST_DAYS INT, EXP_DATE DATETIME); " +
				"CREATE INDEX IDX_EXP_INFO_PROD ON EXP_INFO(PROD_NAME); " +
				"CREATE INDEX IDX_EXP_INFO_ALG ON EXP_INFO(ALG_NAME); ";
				
		int returnValue = BaseDAO.INVALID;
		try {
			ps = BaseDAO.getEvaluationDbConnection().prepareStatement(sql);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public int dropAllAnalysisTables() throws Exception {
		String sql = "DROP TABLE IF EXISTS MTH_TERM_INFO; " +
				"DROP TABLE IF EXISTS MTH_TERM_WGT; " +

				"DROP TABLE IF EXISTS MTH_INFO; " +

				"DROP TABLE IF EXISTS BUG_INFO; " +
				"DROP TABLE IF EXISTS BUG_CMT_INFO; " +
				"DROP TABLE IF EXISTS BUG_TERM_INFO; " +
				"DROP TABLE IF EXISTS BUG_STRACE_INFO; " +
				"DROP TABLE IF EXISTS BUG_MTH_TERM_WGT; " +
				"DROP TABLE IF EXISTS BUG_TERM_WGT; " +
				"DROP TABLE IF EXISTS BUG_FIX_MTH_INFO; " +
				"DROP TABLE IF EXISTS SIMI_BUG_ANAYSIS; " +
				"DROP TABLE IF EXISTS INT_ANALYSIS; " +

				"DROP TABLE IF EXISTS COMM_INFO; " +
				"DROP TABLE IF EXISTS COMM_MTH_INFO; ";
		
		int returnValue = BaseDAO.INVALID;
		try {
			ps = BaseDAO.getAnalysisDbConnection().prepareStatement(sql);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;		
	}
	
	public int dropEvaluationTable() throws Exception {
		String sql = "DROP TABLE IF EXISTS EXP_INFO; ";

		int returnValue = BaseDAO.INVALID;
		try {
			ps = BaseDAO.getEvaluationDbConnection().prepareStatement(sql);
			
			returnValue = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnValue;		
	}
	
	public void initializeAllData() throws Exception {
		boolean commitDataIncluded = true;
		initializeAllData(commitDataIncluded);
	}
	
	public void initializeAllData(boolean commitDataIncluded) throws Exception {
		MethodDAO methodDAO = new MethodDAO();
		
 		methodDAO.deleteAllMethods();
 		methodDAO.deleteAllTerms();
 		methodDAO.deleteAllTermWeights();
		
		BugDAO bugDAO = new BugDAO();
		bugDAO.deleteAllBugs();
		bugDAO.deleteAllComments();
		bugDAO.deleteAllTerms();
		bugDAO.deleteAllBugMthTermWeights();
		bugDAO.deleteAllBugTermWeights();
		bugDAO.deleteAllBugFixedInfo();
		bugDAO.deleteAllSimilarBugInfo();
		
		if (commitDataIncluded) {
			CommitDAO commitDAO = new CommitDAO();
			commitDAO.deleteAllCommitInfo();
			commitDAO.deleteAllCommitMethodInfo();
		}
		
		IntegratedAnalysisDAO integratedAnalysisDAO = new IntegratedAnalysisDAO();
		integratedAnalysisDAO.deleteAllIntegratedAnalysisInfos();
	}
	
	public void initializeAnalysisData() throws Exception {
		IntegratedAnalysisDAO integratedAnalysisDAO = new IntegratedAnalysisDAO();
		integratedAnalysisDAO.deleteAllIntegratedAnalysisInfos();
	}

	public void initializeExperimentResultData() throws Exception {
		ExperimentResultDAO experimentDAO = new ExperimentResultDAO();
		experimentDAO.deleteAllExperimentResults();
	}


}
