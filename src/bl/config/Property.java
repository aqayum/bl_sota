package bl.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;


/**
 * @author Abdul
 *
 */  

public class Property {

	
	final static public int THREAD_COUNT = Integer.parseInt(Property.readProperty("THREAD_COUNT"));
	final static private String WORK_DIR = Property.readProperty("WORK_DIR");
	final static private String OUTPUT_FILE = Property.readProperty("OUTPUT_FILE");
	final static private boolean NEW_BUG_COMMENTS_INCLUDED = Property.readProperty("NEW_BUG_COMMENTS_INCLUDED").equalsIgnoreCase("TRUE");

	

	private String bugFilePath;
	private String sourceCodeDir;
	private String[] sourceCodeDirList;

	private int methodCount;
	private int wordCount;
	private int bugReportCount;
	private int bugTermCount;
	private String pathSeparator=null;
	private double alpha;
	private String separator = System.getProperty("file.separator");
	private String lineSeparator = System.getProperty("line.separator");
	private static Property p = null;
	private String productName;
	private int pastDays;
	private Calendar since = null;
	private Calendar until = null;
	private String repoDir;

	
	private String runLevel;

	public String getPathSeparator() {
		return pathSeparator;
	}

	public void setPathSeparator(String pathSeparator) {
		this.pathSeparator = pathSeparator;
	}
	
	public int getBugTermCount() {
		return bugTermCount;
	}

	public void setBugTermCount(int bugTermCount) {
		this.bugTermCount = bugTermCount;
	}

	public int getBugReportCount() {
		return bugReportCount;
	}

	public void setBugReportCount(int bugReportCount) {
		this.bugReportCount = bugReportCount;
	}

	public int getMethodCount() {
		return methodCount;
	}

	public void setMethodCount(int methodCount) {
		this.methodCount = methodCount;
	}

	public int getWordCount() {
		return wordCount;
	}

	public void setWordCount(int wordCount) {
		this.wordCount = wordCount;
	}

	public String getLineSeparator() {
		return lineSeparator;
	}

	public String getWorkDir() {
		return WORK_DIR;
	}
	
	private static String readProperty(String key) {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("blp.properties"));
		} catch (IOException e) {
		}

		return properties.getProperty(key);
	}
	
	public static void createInstance(String productName, String bugFilePath, String sourceCodeDir, String workDir,
			double alpha, int pastDays, String repoDir, String outputFile, String pathSeparator) {
		if (null == p) {
			p = new Property(productName, bugFilePath, sourceCodeDir, workDir,
					alpha, pastDays, repoDir, outputFile, pathSeparator);
		} else {
			p.setValues(productName, bugFilePath, sourceCodeDir, workDir, alpha,
					pastDays, repoDir, outputFile, pathSeparator);
		}
	}
	
	private Property() {
		// Do nothing
	}
	
	public static Property loadInstance(String targetProduct) throws Exception {
		if (null == p) {
			p = new Property();
		}
		

		
		String product = Property.readProperty("_" + "PRODUCT");
		String pathSeparator = Property.readProperty("_" + "PATHSEPARATER");
		String sourceCodeDir = Property.readProperty("_" + "SOURCE_DIR");
		double alpha = Double.parseDouble(Property.readProperty("_" + "ALPHA"));
		int pastDays = Integer.parseInt(Property.readProperty("_" + "PAST_DAYS"));
		String repoDir = Property.readProperty("_" + "REPO_DIR");
		String bugFilePath = Property.readProperty("_" + "BUG_REPO_FILE");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date sinceDate = dateFormat.parse(Property.readProperty("_" + "COMMIT_SINCE"));
		Calendar since = new GregorianCalendar();
		since.setTime(sinceDate);
		Date untilDate = dateFormat.parse(Property.readProperty("_" + "COMMIT_UNTIL"));
		Calendar until = new GregorianCalendar();
		until.setTime(untilDate);
		
		p.setValues(product, sourceCodeDir, alpha, pastDays, repoDir,
				bugFilePath, since, until, pathSeparator);

		
		return p;
	}
	
	public static Property loadInstance() throws Exception {
		String targetProduct = Property.readProperty("_PRODUCT");
		return loadInstance(targetProduct);
	}
	
	public static Property getInstance() {
		return p;
	}
	
	private Property(String product, String bugFilePath, String sourceCodeDir, String workDir,
			double alpha,  int pastDays, String repoDir, String outputFile, String pathSeparator) {
		setValues(product, bugFilePath, sourceCodeDir, workDir, alpha,
				 pastDays, repoDir, outputFile, pathSeparator);
	}

	private void setValues(String product, String bugFilePath,
			String sourceCodeDir, String workDir, double alpha, int pastDays, String repoDir, String outputFile,String pathSeparator) {

		setValues(product, bugFilePath, sourceCodeDir, workDir, alpha,
				pastDays, repoDir, outputFile, pathSeparator);
	}
	
	private void setValues(String product, String sourceCodeDir,
			double alpha, int pastDays, String repoDir,
			String bugFilePath, Calendar since, Calendar until,String pathSeparator ) {
		
		setProductName(product);
		setSourceCodeDir(sourceCodeDir);		
		sourceCodeDirList = new String[1];
		sourceCodeDirList[0] = sourceCodeDir;
		setAlpha(alpha);
		setPastDays(pastDays);
		setRepoDir(repoDir);
		setBugFilePath(bugFilePath);
		setSince(since);
		setUntil(until);
		setPathSeparator(pathSeparator);

	}
	
	public void printValues() {
		System.out.printf("WORK_DIR: %s\n", Property.WORK_DIR);
		System.out.printf("THREAD_COUNT: %d\n", Property.THREAD_COUNT);
		System.out.printf("OUTPUT_FILE: %s\n\n", Property.OUTPUT_FILE);
		
		System.out.printf("Product name: %s\n", getProductName());
		System.out.printf("Source code dir: %s\n", getSourceCodeDir());
		System.out.printf("Alpha: %f\n", getAlpha());
		System.out.printf("Past days: %s\n", getPastDays());
		System.out.printf("Repo dir: %s\n", getRepoDir());
		System.out.printf("Bug file path: %s\n", getBugFilePath());
		System.out.printf("Source file separator: %s\n", getPathSeparator());
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		System.out.printf("Since: %s\n", dateFormat.format(getSince().getTime()));
		System.out.printf("Until: %s\n", dateFormat.format(getUntil().getTime()));

	}
	
	
	public double getAlpha() {
		return alpha;
	}



	public String getOutputFile() {
		return OUTPUT_FILE;
	}

	public String getBugFilePath() {
		return bugFilePath;
	}

	public String[] getSourceCodeDirList() {
		return sourceCodeDirList;
	}

	public String getSeparator() {
		return separator;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @param alpha the alpha to set
	 */
	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}


	/**
	 * @return the pastDays
	 */
	public int getPastDays() {
		return pastDays;
	}

	/**
	 * @param pastDate the pastDays to set
	 */
	public void setPastDays(int pastDays) {
		this.pastDays = pastDays;
	}

	/**
	 * @return the repoDir
	 */
	public String getRepoDir() {
		return repoDir;
	}

	/**
	 * @param repoDir the repoDir to set
	 */
	public void setRepoDir(String repoDir) {
		this.repoDir = repoDir;
	}
	

	/**
	 * @return the since
	 */
	public Calendar getSince() {
		return since;
	}

	/**
	 * @param since the since to set
	 */
	public void setSince(Calendar since) {
		this.since = since;
	}

	/**
	 * @return the until
	 */
	public Calendar getUntil() {
		return until;
	}

	/**
	 * @param until the until to set
	 */
	public void setUntil(Calendar until) {
		this.until = until;
	}



	/**
	 * @return the sourceCodeDir
	 */
	public String getSourceCodeDir() {
		return sourceCodeDir;
	}

	/**
	 * @param sourceCodeDir the sourceCodeDir to set
	 */
	public void setSourceCodeDir(String sourceCodeDir) {
		this.sourceCodeDir = sourceCodeDir;
	}

	/**
	 * @param bugFilePath the bugFilePath to set
	 */
	public void setBugFilePath(String bugFilePath) {
		this.bugFilePath = bugFilePath;
	}

	/**
	 * @return the runLevel
	 */
	public String getRunLevel() {
		return runLevel;
	}


	/**
	 * @return the newBugCommentsIncluded
	 */
	public boolean isNewBugCommentsIncluded() {
		return NEW_BUG_COMMENTS_INCLUDED;
	}
}
