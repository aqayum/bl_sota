package bl.config;

import bl.analysis.BL_SOTA;
import bl.db.dao.DbUtil;
import bl.evaluation.Evaluator;


/**
 * @author Abdul
 *
 */
public class BLP {
	private static void initializeDB() throws Exception {
		Property prop = Property.getInstance();

		DbUtil dbUtil = new DbUtil();

			dbUtil.openConnetion(prop.getProductName());

			dbUtil.dropAllAnalysisTables();
			dbUtil.createAllAnalysisTables();

			prop.setProductName(prop.getProductName());
			//dbUtil.initializeAllData();

			dbUtil.closeConnection();
		
		
		dbUtil.openEvaluationDbConnection();

		dbUtil.dropEvaluationTable();
		dbUtil.createEvaluationTable();
		
		dbUtil.initializeExperimentResultData();
		
		dbUtil.closeConnection();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		// Load properties data to run BLIA
		Property prop = Property.loadInstance();
		
		// initialize DB and create all tables.
		initializeDB();

		// Run BLIA algorithm
		BL_SOTA blia = new BL_SOTA();
		blia.run();
		


//		// Evaluate the accuracy result of BLIA
		Evaluator evaluator1 = new Evaluator(prop.getProductName(),
                 prop.getAlpha(),
				 prop.getPastDays());
		evaluator1.evaluate();

	}
}
