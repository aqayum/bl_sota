/**
 * Copyright (c) 2014 by Software Engineering Lab. of Sungkyunkwan University. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and distribute this software and its documentation for
 * educational, research, and not-for-profit purposes, without fee and without a signed licensing agreement,
 * is hereby granted, provided that the above copyright notice appears in all copies, modifications, and distributions.
 */
package bl.indexer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import bl.common.Method;
import bl.config.Property;
import bl.db.AnalysisValue;
import bl.db.dao.MethodDAO;

/**
 * @author Klaus Changsun Youm(klausyoum@skku.edu)
 *
 */
public class SourceMethodCreator {
	private HashMap<Integer, Integer> totalCorpusLengths = null;
	private HashMap<Integer, Method> sourceMethodCorpusMap = null;
	private int methodCount = 0; 
	

    public SourceMethodCreator() {
    }
    
	/**
	 * Calculate document counts from source code corpus data
	 *  
	 * @return Hashtable<String, Integer>	Corpus, Term Count 
	 * @throws IOException
	 */
	public Hashtable<String, Integer> getInverseDocCountTable(String version) throws Exception {
		MethodDAO methodDAO= new MethodDAO();

		ArrayList<Method> corpusSets = methodDAO.getAllMethods();
		
		
		Hashtable<String, Integer> countTable = new Hashtable<String, Integer>();
		
			for(int j=0;j<corpusSets.size();j++) {
			String corpuses[] = (corpusSets.get(j).getName()+" "+corpusSets.get(j).getCorpus()).split(" ");
			TreeSet<String> wordSet = new TreeSet<String>();
			for (int i = 0; i < corpuses.length; i++) {
				String word = corpuses[i];
				if (!word.trim().equals("") && !wordSet.contains(word)) {
					wordSet.add(word);
				}

		
			
			Iterator<String> iterator = wordSet.iterator();
			while (iterator.hasNext()) {
				 word = iterator.next();
				if (countTable.containsKey(word)) {
					Integer count = (Integer) countTable.get(word) + 1;
					countTable.remove(word);
					countTable.put(word, count);
				} else {
					countTable.put(word, 1);
				}
			}
			}		
		}
		
		return countTable;
	}	
	
	/**
	 * Compute length score of each source file then write them to LengthScore.txt file  
	 * 
	 * @throws Exception
	 */
	public void computeLengthScore(String version) throws Exception {
		MethodDAO methodDAO= new MethodDAO();

		
		int max = 0x80000000;
		ArrayList<Method> lensTable= methodDAO.getAllMethods();
		//HashMap<String, Integer> lensTable = sourceFileDAO.getTotalCorpusLengths(version);
	
		int count = 0;
		int sum = 0;
		int totalCorpusLength = 0;
		for(int i=0;i<lensTable.size();i++) {
			totalCorpusLength = (lensTable.get(i).getName()+" "+lensTable.get(i).getCorpus()).length();
			if (totalCorpusLength != 0) {
				count++;
			}
			
			if (totalCorpusLength > max) {
				max = totalCorpusLength;
			}
			
			sum += totalCorpusLength;
		}
		// average terms in each file
		double average = (double) sum / (double) count;
		double squareDevi = 0.0D;
		
		for(int i=0;i<lensTable.size();i++)  {
			totalCorpusLength = (lensTable.get(i).getName()+" "+lensTable.get(i).getCorpus()).length();
			if (0 != totalCorpusLength) {
				squareDevi += ((double) totalCorpusLength - average) * ((double) totalCorpusLength - average);
			}
		}
		// standard deviation of terms
		double standardDevi = Math.sqrt(squareDevi / (double) count);
		double low = average - 3D * standardDevi;
		double high = average + 3D * standardDevi;
		int min = 0;
		if (low > 0.0D) {
			min = (int) low;
		}
		
		for(int i=0;i<lensTable.size();i++)  {
			totalCorpusLength = (lensTable.get(i).getName()+" "+lensTable.get(i).getCorpus()).length();
			double score = 0.0D;
			double nor = getNormalizedValue(totalCorpusLength, high, min);
			if (totalCorpusLength != 0) {
				if ((double) totalCorpusLength > low && (double) totalCorpusLength < high) {
					score = getLengthScore(nor);
				} else if ((double) totalCorpusLength < low) {
					score = 0.5D;
				} else {
					score = 1.0D;
				}
			} else {
				score = 0.0D;
			}
			if (nor > 6D) {
				nor = 6D;
			}
			if (score < 0.5D) {
				score = 0.5D;
			}
			//bring the term count value in a method into a normalized range from 0.5-1
//			System.out.printf("FileName: %s, score: %f\n", fileName, score);
			methodDAO.updateLengthScore(lensTable.get(i).getID(), score, totalCorpusLength);

		}
	}

	/**
	 * Get normalized value of x from Max. to min.
	 * 
	 * @param x
	 * @param max
	 * @param min
	 * @return
	 */
	private double getNormalizedValue(int x, double max, double min) {
		return (6F * (x - min)) / (max - min);
	}

	/**
	 * Get length score of BugLocator
	 * 
	 * @param len
	 * @return
	 */
	public double getLengthScore(double len) {
		return Math.exp(len) / (1.0D + Math.exp(len));
	}
	
	/* (non-Javadoc)
	 * @see edu.skku.selab.blia.indexer.IIndexer#createIndex()
	 */
	public void createIndex(String version) throws Exception {
		Property property = Property.getInstance();
		// inverseDocCountTable contain the information of each word and and its total frequency across complete corpus
		Hashtable<String, Integer> inverseDocCountTable = getInverseDocCountTable(version);
		// set total word count
		property.setWordCount(inverseDocCountTable.size());	
		
		MethodDAO methodDAO= new MethodDAO();

		// insert corpus
		String term = "";
		Iterator<String> idcTableIter = inverseDocCountTable.keySet().iterator();
		while (idcTableIter.hasNext()) {
			term = idcTableIter.next();
			methodDAO.insertTerm(term);
		}
		// the corpus object of each class, becuase here we are not dealing with method level we didn't fetch MethodList in SourceFileCorpus objects
		ArrayList<Method> corpusMap= methodDAO.getAllMethods();

		


		int termCount = 0;
		int inverseDocCount = 0;
		for(int j=0; j<corpusMap.size();j++) {
			// get term count
			String termArray[] = (corpusMap.get(j).getName()+" " +corpusMap.get(j).getCorpus()).split(" ");

			Hashtable<String, Integer> termTable = new Hashtable<String, Integer>();
			for (int i = 0; i < termArray.length; i++) {
				term = termArray[i];
				if (!term.trim().equals("")) {

					if (termTable.containsKey(term)) {
						Integer count = (Integer) termTable.get(term);
						count = Integer.valueOf(count.intValue() + 1);
						termTable.remove(term);
						termTable.put(term, count);
					} else {
						termTable.put(term, Integer.valueOf(1));
					}
				}
			}
			// Now termTable contains term and its total frequency for each (one) method
			

			Iterator<String> termTableIter = termTable.keySet().iterator();
			while (termTableIter.hasNext()) {
				term = termTableIter.next();
				termCount = termTable.get(term);// local frequency (method wise)
				inverseDocCount = inverseDocCountTable.get(term).intValue();// total frequency (complete corpus wise)
				// just create an object of AnalysisValue to insert in db
				AnalysisValue termWeight = new AnalysisValue(corpusMap.get(j).getID(), version, term, termCount, inverseDocCount);
				// insert the object in weight Table
				methodDAO.insertTermWeight(termWeight);

			}
		}
	}
    
    public static HashSet<String> CorpusToSet(String corpus) {
    	HashSet<String> termSet = new HashSet<String>();
    	
    	String[] stringArray = corpus.split(" ");
   		for (int i = 0; i < stringArray.length; i++) {
   			termSet.add(stringArray[i]);
    	}
    	
    	return termSet;
    }
    
    private class WorkerThread implements Runnable {
    	private int methodID;

    	
    	
        public WorkerThread(int methodID){
            this.methodID = methodID;

        }
     
        @Override
        public void run() {
			// Compute similarity between Bug report & source files
        	
        	try {
        		insertDataToDb();
        	} catch (Exception e) {
        		e.printStackTrace();
        	}
        }
        
        private void insertDataToDb() throws Exception {
        	MethodDAO methodDAO= new MethodDAO();
        	
//			if (fileName.equalsIgnoreCase("org.eclipse.swt.internal.win32.NMCUSTOMDRAW.java")) {
				Integer totalTermCount = totalCorpusLengths.get(methodID);
				
				HashMap<String, AnalysisValue> sourceMethodTermMap = methodDAO.getTermMap(methodID);
				if (sourceMethodTermMap == null) {
					// debug code
					System.out.printf("[SourceMethodVectorCreator.create()] The Method name that has no valid terms: %s\n", methodID);
					return;
				}

				double corpusNorm = 0.0D;
				double corpusNormCL = 0.0D;
	
				Method sourceMethod = sourceMethodCorpusMap.get(methodID);
				HashSet<String> methodTermSet = CorpusToSet(sourceMethod.getName()+" "+sourceMethod.getCorpus());
			
				
				Iterator<String> methodTermIter = methodTermSet.iterator();
				while (methodTermIter.hasNext()) {
					String term = methodTermIter.next();
					AnalysisValue termWeight = sourceMethodTermMap.get(term);
					double tf = getTfValue(termWeight.getTermCount());
					double tfClassic = getTfValue(termWeight.getTermCount(), totalTermCount.intValue());
					double idf = getIdfValue(termWeight.getInvDocCount(), methodCount);
					double termWeightValue = (tf * idf);
					double termWeightValueSquare = termWeightValue * termWeightValue;
					double termWeightValueCL = (tfClassic * idf);
					double termWeightValueSquareCL = termWeightValueCL * termWeightValueCL;
//					System.out.printf("term: %s, termCount: %d, documentCount: %d, tf: %f, idf: %f, termWeight: %f\n",
//							term, termWeight.getTermCount(), termWeight.getInvDocCount(), tf, idf, termWeightValue);
					corpusNorm += termWeightValueSquare;
					corpusNormCL+=termWeightValueSquareCL;
					termWeight.setTfClassic(tfClassic);
					termWeight.setTf(tf);
					termWeight.setIdf(idf);
					methodDAO.updateTermWeight(termWeight);
				}
				corpusNorm = Math.sqrt(corpusNorm);
				corpusNormCL = Math.sqrt(corpusNormCL);

//				System.out.printf(">>>> corpusNorm: %f, classCorpusNorm: %f, methodCorpusNorm: %f, variableNorm: %f, commentNorm: %f\n",
//						corpusNorm, classCorpusNorm, methodCorpusNorm, variableNorm, variableNorm);
				
				methodDAO.updateNormValues(methodID, corpusNorm, corpusNormCL);
        }
    }


	/* (non-Javadoc)
	 * @see edu.skku.selab.blia.indexer.IVectorCreator#create()
	 */
	public void create(String version) throws Exception {
		MethodDAO methodDAO= new MethodDAO();
		totalCorpusLengths = methodDAO.getTotalCorpusLengths();
		sourceMethodCorpusMap = methodDAO.getAllMethodsMap();
		methodCount = methodDAO.getSourceMethodCount();
		
		// Calculate vector
		Iterator<Integer> methodNameIter = totalCorpusLengths.keySet().iterator();
		ExecutorService executor = Executors.newFixedThreadPool(Property.THREAD_COUNT);
		while (methodNameIter.hasNext()) {
			int methodID = methodNameIter.next();
			Runnable worker = new WorkerThread(methodID);
			executor.execute(worker);
		}
		
		executor.shutdown();
		while (!executor.isTerminated()) {
		}
	}
	
	private float getTfValue(int freq) {
		return (float) Math.log(freq) + 1.0F;
	}
	
	private double getTfValue(double freq, double totalTermCount) {
		return (double) (freq/totalTermCount);
	}

	private float getIdfValue(double docCount, double totalCount) {
		return (float) Math.log(totalCount / docCount);
	}

}
