/**
 * Copyright (c) 2014 by Software Engineering Lab. of Sungkyunkwan University. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and distribute this software and its documentation for
 * educational, research, and not-for-profit purposes, without fee and without a signed licensing agreement,
 * is hereby granted, provided that the above copyright notice appears in all copies, modifications, and distributions.
 */
package bl.indexer;

import java.io.File;
import java.util.ArrayList;
import java.util.TreeSet;

import bl.common.FileDetector;
import bl.common.FileParser;
import bl.common.Method;
import bl.common.SourceMethodCorpus;
import bl.config.Property;
import bl.db.dao.MethodDAO;


/**
 * @author Klaus Changsun Youm(klausyoum@skku.edu)
 *
 */
public class StructuredSourceFileCorpusCreator  {
	public SourceMethodCorpus create(File file) {
		FileParser parser = new FileParser(file);
		String fileName = parser.getPackageName();
		if (fileName.trim().equals("")) {
			fileName = file.getName();
		} else {
			fileName = (new StringBuilder(String.valueOf(fileName)))
					.append(".").append(file.getName()).toString();
		}
		fileName = fileName.substring(0, fileName.lastIndexOf("."));
		
		
		ArrayList<Method> methodList =  parser.getAllMethodList();

		
		SourceMethodCorpus corpus = new SourceMethodCorpus();
		corpus.setJavaFilePath(file.getAbsolutePath());
		corpus.setJavaFileFullClassName(fileName);
		corpus.setMethodList(methodList);
		return corpus;
    }
	
	////////////////////////////////////////////////////////////////////	
	/* (non-Javadoc)
	 * @see edu.skku.selab.blia.indexer.ICorpus#create()
	 */
	public void create(String version) throws Exception {
		Property property = Property.getInstance();
		FileDetector detector = new FileDetector("java");
		File files[] = detector.detect(property.getSourceCodeDirList());
		
		MethodDAO methodDAO = new MethodDAO();

		int count = 0;
		TreeSet<String> nameSet = new TreeSet<String>();
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			SourceMethodCorpus corpus = create(file);

			if (corpus != null && !nameSet.contains(corpus.getJavaFileFullClassName())) {
				String className = corpus.getJavaFileFullClassName();
				if (!corpus.getJavaFileFullClassName().endsWith(".java")) {
					className += ".java";
				}
				
				String fileName = "";
				// i think next general code is possible where all products could be handled
				if (!property.getPathSeparator().toString().trim().equals(".")) {
					String absolutePath = file.getAbsolutePath();
					String sourceCodeDirName = property.getSourceCodeDir();
					int index = absolutePath.indexOf(sourceCodeDirName);
					fileName = absolutePath.substring(index + sourceCodeDirName.length() + 1, absolutePath.length());
					fileName = fileName.replace("\\", "/");
				
//					System.out.printf("[StructuredSourceFileCorpusCreator.create()] %s, %s\n", filePath, fileName);
				} else {
					fileName = file.getAbsolutePath().replace("\\", ".");
					fileName = fileName.replace("/", ".");
					
					// Wrong file that has invalid package or path
					if (!fileName.endsWith(className)) {
						System.err.printf("[StructuredSourceFileCorpusCreator.create()] %s, %s\n", fileName, className);
						continue;
					}
					
					fileName = className;
				}
				
				ArrayList<Method> methodList = corpus.getMethodList();
				for (int j = 0; j < methodList.size(); ++j) {
					Method method = methodList.get(j);
					method.setSourceFileName(fileName);
					methodDAO.insertMethod(method, version);
					count++;// counter of number of methods
				}

				nameSet.add(corpus.getJavaFileFullClassName());//nameset contains classes and not methods
				//count++;// counter of number of files
			}
		}

		property.setMethodCount(count);
	}
}
