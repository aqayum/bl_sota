package bl.common;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * @author Abdul
 *
 */
public class Method {
    private int ID;
    private String name;
    private String returnType;
    private String params;
    private String hashKey;
    private String corpus;
    private String sourceFileName;
    
    public String getSourceFileName() {
		return sourceFileName;
	}

	public void setSourceFileName(String sourceFileName) {
		this.sourceFileName = sourceFileName;
	}

	public String getCorpus() {
		return corpus;
	}

	public void setCorpus(String corpus) {
		this.corpus = corpus;
	}

	public Method() {
    	this.setID(0);
    	this.setName("");
    	this.setReturnType("");
    	this.setParams("");
    	this.setHashKey("");
    }
    
    public Method(String concatenatedMethodInfo) {
    	this.setID(0);
   	
    	String splitLines[] = concatenatedMethodInfo.split("\\|");
    	this.setName(splitLines[0]);
    	this.setReturnType("");
    	this.setParams("");
    	if (splitLines.length > 1) {
        	this.returnType = splitLines[1];
        	
        	this.params = (splitLines.length < 3) ? "" : splitLines[2];
    	}
    	this.setHashKey(calculateMD5(name + " " + returnType + " " + params));
    }
    
    public Method(String name, String returnType, String params, String corpus) {
    	this.setID(0);

    	this.name = name;
    	this.returnType = returnType;
    	this.params = params;
    	this.corpus=corpus;
    	this.setHashKey(calculateMD5(name + " " + returnType + " " + params));
    }
    
    public Method(int methodID, String fileName,  String name, String returnType, String params, String corpus, String hashKey) {
    	this.setID(methodID);
    	this.sourceFileName=fileName;

    	this.name = name;
    	this.returnType = returnType;
    	this.params = params;
    	this.corpus=corpus;
    	this.setHashKey(hashKey);
    }
    
    private String calculateMD5(String str){
    	String MD5 = ""; 
    	try{
    		MessageDigest md = MessageDigest.getInstance("MD5"); 
    		md.update(str.getBytes()); 
    		byte byteData[] = md.digest();
    		StringBuffer sb = new StringBuffer(); 
    		for(int i = 0 ; i < byteData.length ; i++){
    			sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
    		}
    		MD5 = sb.toString();
    		
    	}catch(NoSuchAlgorithmException e){
    		e.printStackTrace(); 
    		MD5 = null; 
    	}
    	return MD5;
    }

	/**
	 * @return the ID
	 */
	public int getID() {
		return ID;
	}

	/**
	 * @param ID the ID to set
	 */
	public void setID(int ID) {
		this.ID = ID;
	}



	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the returnType
	 */
	public String getReturnType() {
		return returnType;
	}

	/**
	 * @param returnType the returnType to set
	 */
	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	/**
	 * @return the argTypes
	 */
	public String getParams() {
		return params;
	}

	/**
	 * @param params the argTypes to set
	 */
	public void setParams(String params) {
		this.params = params;
	}

	/**
	 * @return the hashKey
	 */
	public String getHashKey() {
		return hashKey;
	}

	/**
	 * @param hashKey the hashKey to set
	 */
	public void setHashKey(String hashKey) {
		this.hashKey = hashKey;
	}
	
	public String getConcatenatedString() {
		return name + "|" + returnType + "|" + params;
	}
	
	public boolean equals(Object obj) {
		Method targetMethod = (Method) obj;
		return (this.getName().equals(targetMethod.getName()) &&
				this.getReturnType().equals(targetMethod.getReturnType()) &&
				this.getParams().equals(targetMethod.getParams()));
	}
}
