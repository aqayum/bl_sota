package bl.common;

import java.util.ArrayList;

/**
 * @author Abdul
 *
 */
public class SourceMethodCorpus {
	private String javaFileFullClassName;
	private String javaFilePath;
	private String methodPart;

	

	private ArrayList<Method> methodList;
	private double methodCorpusNorm;

	public SourceMethodCorpus() {
		javaFileFullClassName = "";
		javaFilePath = "";
		methodPart = "";
		setMethodList(null);
		methodCorpusNorm = 0;
	}

	public String getJavaFileFullClassName() {
		return javaFileFullClassName;
	}

	public void setJavaFileFullClassName(String javaFileFullClassName) {
		this.javaFileFullClassName = javaFileFullClassName;
	}

	public String getJavaFilePath() {
		return javaFilePath;
	}

	public void setJavaFilePath(String javaFilePath) {
		this.javaFilePath = javaFilePath;
	}

	/**
	 * @return the methodPart
	 */
	public String getMethodPart() {
		return methodPart;
	}

	/**
	 * @param methodPart the methodPart to set
	 */
	public void setMethodPart(String methodPart) {
		this.methodPart = methodPart;
	}

	/**
	 * @return the methodCorpusNorm
	 */
	public double getMethodCorpusNorm() {
		return methodCorpusNorm;
	}

	/**
	 * @param methodCorpusNorm the methodCorpusNorm to set
	 */
	public void setMethodCorpusNorm(double methodCorpusNorm) {
		this.methodCorpusNorm = methodCorpusNorm;
	}

	/**
	 * @return the methodList
	 */
	public ArrayList<Method> getMethodList() {
		return methodList;
	}

	/**
	 * @param methodList the methodList to set
	 */
	public void setMethodList(ArrayList<Method> methodList) {
		this.methodList = methodList;
	}
}
