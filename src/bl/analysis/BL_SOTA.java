package bl.analysis;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import bl.common.Bug;
import bl.config.Property;
import bl.db.IntegratedAnalysisValue;
import bl.db.dao.BugDAO;
import bl.db.dao.DbUtil;
import bl.db.dao.IntegratedAnalysisDAO;
import bl.db.dao.MethodDAO;
import bl.indexer.BugCorpusCreator;
import bl.indexer.BugMethodVectorCreator;
import bl.indexer.BugVectorCreator;
import bl.indexer.GitCommitLogCollector;
import bl.indexer.SourceMethodCreator;
import bl.indexer.StructuredSourceFileCorpusCreator;
import bl.utils.Util;

/**
 * @author Abdul 
 *
 */
public class BL_SOTA {
	private final String version = MethodDAO.DEFAULT_VERSION_STRING;
	private ArrayList<Bug> bugs = null;
	private double alpha = 0;

	
	public BL_SOTA() {
		prepareWorkingDir();
	}
	
	private String getElapsedTimeSting(long startTime) {
		long elapsedTime = System.currentTimeMillis() - startTime;
		String elpsedTimeString = (elapsedTime / 1000) + "." + (elapsedTime % 1000);
		return elpsedTimeString;
	}
	
	public void prepareAnalysisData(Date commitSince, Date commitUntil) throws Exception {
		System.out.printf("[STARTED] Method corpus creating.\n");
		long startTime = System.currentTimeMillis();
			StructuredSourceFileCorpusCreator structuredSourceFileCorpusCreator = new StructuredSourceFileCorpusCreator();
			structuredSourceFileCorpusCreator.create(version);
		System.out.printf("[DONE] Method corpus creating.(%s sec)\n", getElapsedTimeSting(startTime));

		System.out.printf("[STARTED] Method vector creating.\n");
		startTime = System.currentTimeMillis();
		SourceMethodCreator methodVectorCreator = new SourceMethodCreator();
		// calculate term frequency, term in a document frequeny and insert term count in table
		methodVectorCreator.createIndex(version);
		//bring the term count value in a method into a normalized range from 0.5-1 and insert the normalized score in SF_VER table
		methodVectorCreator.computeLengthScore(version);
		// fill Analysis.. object with tf and idf in threads
		methodVectorCreator.create(version);
		System.out.printf("[DONE] Method vector creating.(%s sec)\n", getElapsedTimeSting(startTime));
		
		// Create SordtedID.txt
		System.out.printf("[STARTED] Bug corpus creating.\n");
		startTime = System.currentTimeMillis();
		BugCorpusCreator bugCorpusCreator = new BugCorpusCreator();
		boolean stackTraceAnaysis = false;
		// read the XML files (already provided) and fill the database for Bugs information
		bugCorpusCreator.create(stackTraceAnaysis);
		System.out.printf("[DONE] Bug corpus creating.(%s sec)\n", getElapsedTimeSting(startTime));
		
		System.out.printf("[STARTED] Bug vector creating.\n");
		startTime = System.currentTimeMillis();
		BugVectorCreator bugVectorCreator = new BugVectorCreator();
		// fill information against each term of bug
		bugVectorCreator.create();
		System.out.printf("[DONE] Bug vector creating.(%s sec)\n", getElapsedTimeSting(startTime));

		System.out.printf("[STARTED] Commit log collecting.\n");
		startTime = System.currentTimeMillis();
		String repoDir = Property.getInstance().getRepoDir();
		GitCommitLogCollector gitCommitLogCollector = new GitCommitLogCollector(repoDir);
		
		boolean collectForcely = false;
		gitCommitLogCollector.collectCommitLog(commitSince, commitUntil, collectForcely);
		System.out.printf("[DONE] Commit log collecting.(%s sec)\n", getElapsedTimeSting(startTime));
		
		System.out.printf("[STARTED] Bug-Source method vector creating.\n");
		startTime = System.currentTimeMillis();
		BugMethodVectorCreator bugMethodVectorCreator = new BugMethodVectorCreator(); 
		// from .git file it collect the commit information then developed the gold set
		bugMethodVectorCreator.create(version);
		System.out.printf("[DONE] Bug-Source  method creating.(%s sec)\n", getElapsedTimeSting(startTime));
	}
	
	public void preAnalyze() throws Exception {
		BugDAO bugDAO = new BugDAO();
		boolean orderedByFixedDate = true;
		bugs = bugDAO.getAllBugs(orderedByFixedDate);

		// VSM_SCORE
		System.out.printf("[STARTED] Method analysis.\n");//
		long startTime = System.currentTimeMillis();
		SourceMethodAnalyzer sourceMethodAnalyzer = new SourceMethodAnalyzer(bugs);//
		sourceMethodAnalyzer.analyze(version);
		System.out.printf("[DONE] Method analysis.(%s sec)\n", getElapsedTimeSting(startTime));//

		// SIMI_SCORE
		System.out.printf("[STARTED] Bug repository analysis.\n");
		startTime = System.currentTimeMillis();
		BugRepoAnalyzer bugRepoAnalyzer = new BugRepoAnalyzer(bugs);
		//insert inter bug similarity
		bugRepoAnalyzer.analyze();
		System.out.printf("[DONE] Bug repository analysis.(%s sec)\n", getElapsedTimeSting(startTime));
		
		// COMM_SCORE
		System.out.printf("[STARTED] Scm repository analysis.\n");
		startTime = System.currentTimeMillis();
		ScmRepoAnalyzer scmRepoAnalyzer = new ScmRepoAnalyzer(bugs);
		scmRepoAnalyzer.analyze(version);
		System.out.printf("[DONE] Scm repository analysis.(%s sec)\n", getElapsedTimeSting(startTime));
	}
	
    
    private void calculateBLScore(int bugID, boolean includeStackTrace) throws Exception {
    	IntegratedAnalysisDAO integratedAnalysisDAO = new IntegratedAnalysisDAO();
    	
		HashMap<Integer, IntegratedAnalysisValue> integratedAnalysisValues = integratedAnalysisDAO.getAnalysisValues(bugID);
		if (null == integratedAnalysisValues) {
			return;
		}
		
		normalize(integratedAnalysisValues);
		combine(integratedAnalysisValues, alpha, includeStackTrace);
		
		@SuppressWarnings("unused")
		int methodCount = integratedAnalysisValues.keySet().size();
		Iterator<Integer> integratedAnalysisValuesIter = integratedAnalysisValues.keySet().iterator();
		while (integratedAnalysisValuesIter.hasNext()) {
			int methodID = integratedAnalysisValuesIter.next();
			
			IntegratedAnalysisValue integratedAnalysisValue = integratedAnalysisValues.get(methodID);
			int updatedColumnCount = integratedAnalysisDAO.updateBugLocatorScore(integratedAnalysisValue);
			if (0 == updatedColumnCount) {
				System.err.printf("[ERROR]  BugLocator score update failed! BugID: %s, MethodID: %d\n",
						integratedAnalysisValue.getBugID(), integratedAnalysisValue.getSourceMethodVersionID());
			}
		}
    }
    
    
	
	public void analyze(String version, boolean includeStackTrace, boolean includeMethodAnalyze) throws Exception {
		if (null == bugs) {
			BugDAO bugDAO = new BugDAO();
			bugs = bugDAO.getAllBugs(false);			
		}
		
		Property property = Property.getInstance();
		alpha = property.getAlpha();

		
		System.out.printf("[STARTED] calculateBugLocatorScore.anlayze()\n");
		for (int i = 0; i < bugs.size(); i++) {
			long startTime = System.currentTimeMillis();
			int bugID = bugs.get(i).getID();
			calculateBLScore(bugID, includeStackTrace);
			System.out.printf("[calculateBugLocatorScore()] [%d] Bug ID: %d (%s sec)\n", i, bugID, Util.getElapsedTimeSting(startTime));
		}
		
		
		
		System.out.printf("[DONE] calculateBugLocatorScore.anlayze()\n");
	}
	
	/**
	 * 
	 * @param integratedAnalysisValues
	 */
	private void combine(HashMap<Integer, IntegratedAnalysisValue> integratedAnalysisValues, double alpha, 
			boolean includeStackTrace) {
		Iterator<Integer> integratedAnalysisValuesIter = integratedAnalysisValues.keySet().iterator();
		while (integratedAnalysisValuesIter.hasNext()) {
			int methodID = integratedAnalysisValuesIter.next();
			IntegratedAnalysisValue integratedAnalysisValue = integratedAnalysisValues.get(methodID);
			
			double vsmScore = integratedAnalysisValue.getVsmScore();
			double similarityScore = integratedAnalysisValue.getSimilarityScore();

			
			double bugLocatorScore = (1 - alpha) * (vsmScore) + alpha * similarityScore;
			integratedAnalysisValue.setBugLocatorScore(bugLocatorScore);
			
		}
	}
	
	/**
	 * Normalize values in array from max. to min of array
	 * 
	 * @param array
	 * @return
	 */
	private void normalize(HashMap<Integer, IntegratedAnalysisValue> integratedAnalysisValues) {
		double maxVsmScore = Double.MIN_VALUE;
		double minVsmScore = Double.MAX_VALUE;;
		double maxSimiScore = Double.MIN_VALUE;
		double minSimiScore = Double.MAX_VALUE;;
//		double maxCommitLogScore = Double.MIN_VALUE;
//		double minCommitLogScore = Double.MAX_VALUE;;

		
		Iterator<Integer> integratedAnalysisValuesIter = integratedAnalysisValues.keySet().iterator();
		while (integratedAnalysisValuesIter.hasNext()) {
			int sourceFileVersionID = integratedAnalysisValuesIter.next();
			IntegratedAnalysisValue integratedAnalysisValue = integratedAnalysisValues.get(sourceFileVersionID);
			double vsmScore = integratedAnalysisValue.getVsmScore();
			double simiScore = integratedAnalysisValue.getSimilarityScore();
//			double commitLogScore = integratedAnalysisValue.getCommitLogScore();
			if (maxVsmScore < vsmScore) {
				maxVsmScore = vsmScore;
			}
			if (minVsmScore > vsmScore) {
				minVsmScore = vsmScore;
			}
			if (maxSimiScore < simiScore) {
				maxSimiScore = simiScore;
			}
			if (minSimiScore > simiScore) {
				minSimiScore = simiScore;
			}
//			if (maxCommitLogScore < commitLogScore) {
//				maxCommitLogScore = commitLogScore;
//			}
//			if (minCommitLogScore > commitLogScore) {
//				minCommitLogScore = commitLogScore;
//			}	
		}
		
		double spanVsmScore = maxVsmScore - minVsmScore;
		double spanSimiScore = maxSimiScore - minSimiScore;
//		double spanCommitLogScore = maxCommitLogScore - minCommitLogScore;
		integratedAnalysisValuesIter = integratedAnalysisValues.keySet().iterator();
		while (integratedAnalysisValuesIter.hasNext()) {
			int sourceFileVersionID = integratedAnalysisValuesIter.next();
			IntegratedAnalysisValue integratedAnalysisValue = integratedAnalysisValues.get(sourceFileVersionID);
			double normalizedVsmScore = (integratedAnalysisValue.getVsmScore() - minVsmScore) / spanVsmScore;
			double normalizedSimiScore = (integratedAnalysisValue.getSimilarityScore() - minSimiScore) / spanSimiScore;
//			double normalizedCommitLogScore = (integratedAnalysisValue.getCommitLogScore() - minCommitLogScore) / spanCommitLogScore;
			integratedAnalysisValue.setVsmScore(normalizedVsmScore);
			integratedAnalysisValue.setSimilarityScore(normalizedSimiScore);
//			integratedAnalysisValue.setCommitLogScore(normalizedCommitLogScore);
		}
	}
	
	/**
	 * Normalize values of VSM score ONLY in array from max. to min of array 
	 * 
	 * @param array
	 * @return
	 */
    private boolean deleteDirectory(File path) {
        if(!path.exists()) {
            return false;
        }
         
        File[] files = path.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                deleteDirectory(file);
            } else {
                file.delete();
            }
        }
         
        return path.delete();
    }
	
	private void prepareWorkingDir() {
		Property prop = Property.getInstance();
		String workDir = prop.getWorkDir();
		
		File dir = new File(workDir);
		if (dir.exists()) {
			deleteDirectory(dir);
		}
		
		if (false == dir.mkdir()) {
			System.err.println(workDir + " can't be created!");
			
			if (false == dir.mkdir()) {
				System.err.println(workDir + " can't be created again");
			}
		}
	}
	
	public void run() throws Exception {
		Property prop = Property.getInstance();
		prepareWorkingDir();
				
		long startTime = System.currentTimeMillis();
		BL_SOTA blia = new BL_SOTA();
		
		boolean includeStackTrace = true;
		
		boolean includeMethodAnalyze = true;

		DbUtil dbUtil = new DbUtil();
		String dbName = prop.getProductName();
		dbUtil.openConnetion(dbName);
		boolean commitDataIncluded = true;
		dbUtil.initializeAllData(commitDataIncluded);
		dbUtil.closeConnection();

		startTime = System.currentTimeMillis();	
		System.out.printf("[STARTED] prepareAnalysisData().\n");
		blia.prepareAnalysisData(prop.getSince().getTime(), prop.getUntil().getTime());
		System.out.printf("[DONE] prepareAnalysisData().(Total %s sec)\n", Util.getElapsedTimeSting(startTime));
		// little problem at line 111-112 in ScmRepoAnalyzer. Bugid+Methodid (primary key duplication)	
		System.out.printf("[STARTED] pre-anlaysis.\n");
		blia.preAnalyze();
		System.out.printf("[DONE] pre-anlaysis.(Total %s sec)\n", Util.getElapsedTimeSting(startTime));
		
		System.out.printf("[STARTED] anlaysis.\n");
		startTime = System.currentTimeMillis();
		blia.analyze(version, includeStackTrace, includeMethodAnalyze);
		System.out.printf("[DONE] anlaysis.(Total %s sec)\n", Util.getElapsedTimeSting(startTime));
	}
}
