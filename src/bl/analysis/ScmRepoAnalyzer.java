package bl.analysis;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import bl.common.Bug;
import bl.common.ExtendedCommitInfo;
import bl.common.Method;
import bl.config.Property;
import bl.db.ExtendedIntegratedAnalysisValue;
import bl.db.IntegratedAnalysisValue;
import bl.db.dao.BaseDAO;
import bl.db.dao.CommitDAO;
import bl.db.dao.IntegratedAnalysisDAO;
import bl.db.dao.MethodDAO;


/**
 * @author Abdul
 *
 */
public class ScmRepoAnalyzer {
	private ArrayList<Bug> bugs;
	private int pastDays;
	private ArrayList<ExtendedCommitInfo> filteredCommitInfos = null;
	
	public ScmRepoAnalyzer() {
		bugs = null;
	}
	
    public ScmRepoAnalyzer(ArrayList<Bug> bugs) {
    	this.bugs = bugs;
		pastDays = Property.getInstance().getPastDays();
    }
    
    private class WorkerThread implements Runnable {
    	private Bug bug;
    	private String version;
    	
        public WorkerThread(Bug bug, String version) {
            this.bug = bug;
            this.version = version;
        }
     
        @Override
        public void run() {
			// Compute similarity between Bug report & source files
        	
        	try {
        		insertDataToDb();
        	} catch (Exception e) {
        		e.printStackTrace();
        	}
        }
        
        private void insertDataToDb() throws Exception {
    		IntegratedAnalysisDAO integratedAnalysisDAO = new IntegratedAnalysisDAO();
    		MethodDAO methodDAO = new MethodDAO();
    		
			// <fileName, analysisValue>
			HashMap<Integer, IntegratedAnalysisValue> analysisValues = new HashMap<Integer, IntegratedAnalysisValue>();
			//HashMap<Integer, ExtendedIntegratedAnalysisValue> methodAnalysisValues = new HashMap<Integer, ExtendedIntegratedAnalysisValue>();
			ArrayList<ExtendedCommitInfo> relatedCommitInfos = findCommitInfoWithinDays(filteredCommitInfos, bug.getOpenDate(), pastDays);
			if (null == relatedCommitInfos) {
				return;
			}
			
			for (int j = 0; j < relatedCommitInfos.size(); j++) {
				ExtendedCommitInfo relatedCommitInfo = relatedCommitInfos.get(j);
				HashMap<String, ArrayList<Method>> allCommitMethods = relatedCommitInfo.getAllFixedMethods();
				if (allCommitMethods == null) {
//					System.err.printf("[NO fixed method!] BugID: %d, Commit ID: %s\n", bug.getID(), relatedCommitInfo.getCommitID());
					continue;
				}
				Iterator<String> commitMethodsIter = allCommitMethods.keySet().iterator();
				while (commitMethodsIter.hasNext()) {
					String commitMethodName = commitMethodsIter.next();
					ArrayList<Method> commitMethods = allCommitMethods.get(commitMethodName);
					
					for (int i = 0; i < commitMethods.size(); ++i) {
						Method method = commitMethods.get(i);
						int methodID = methodDAO.getMethodID(method);

						if (methodID == BaseDAO.INVALID) {
							methodID = methodDAO.insertMethod(method);
						}
						IntegratedAnalysisValue analysisValue = analysisValues.get(methodID);
					   if (null == analysisValue) {
						   analysisValue = new ExtendedIntegratedAnalysisValue();
						   analysisValue.setBugID(bug.getID());
						   analysisValue.setMethodID(methodID);
						   analysisValue.setMethodName(method.getName());
						   analysisValue.setVersion(version);
						}

						// Calculate CommitLogScore for method level
						double commitLogScore = analysisValue.getCommitLogScore();
						commitLogScore += calculateCommitLogScore(relatedCommitInfo.getCommitDate(), bug.getOpenDate(), pastDays);
						analysisValue.setCommitLogScore(commitLogScore);
						if (null == analysisValues.get(methodID)) {
							analysisValues.put(methodID, analysisValue);		
						}
						
					}

				}
			}
			Iterator<Integer> methodAnalysisValueIter = analysisValues.keySet().iterator();
			while (methodAnalysisValueIter.hasNext()) {
				int methodID = methodAnalysisValueIter.next();
				IntegratedAnalysisValue analysisValue = analysisValues.get(methodID);
				int updatedColumenCount = integratedAnalysisDAO.updateCommitLogScore(methodID, analysisValue);
				
				if (0 == updatedColumenCount) {
					integratedAnalysisDAO.insertAnalysisVaule(analysisValue);
				}
				

			}
		}
    }

	public void analyze(String version) throws Exception {
		// Do loop from the oldest bug,
		CommitDAO commitDAO = new CommitDAO();
		
		// Checked the "filtered". This variable is valid when it is true
		boolean filtered = true;
		filteredCommitInfos = commitDAO.getCommitInfos(filtered);

		ExecutorService executor = Executors.newFixedThreadPool(Property.THREAD_COUNT);
		for (int i = 0; i < bugs.size(); i++) {
			Runnable worker = new WorkerThread(bugs.get(i), version);
			executor.execute(worker);
		}
		
		executor.shutdown();
		while (!executor.isTerminated()) {
		}
	}
	
	private double calculateCommitLogScore(Date commitDate, Date openDate, Integer pastDays) {
		double diffDays = getDiffDays(commitDate, openDate);
		double returnValue = 1.0 / (1 + Math.exp(12 * (1 - ((pastDays - diffDays) / pastDays))));		
		return returnValue;
	}

	private double getDiffDays(Date sourceDate, Date targetDate) {
		long diff = targetDate.getTime() - sourceDate.getTime();
	    double diffDays = diff / (24.0 * 60 * 60 * 1000);
		
	    return diffDays;
	}

	private ArrayList<ExtendedCommitInfo> findCommitInfoWithinDays(ArrayList<ExtendedCommitInfo> allCommitInfo, Date openDate, Integer pastDays) {
		ArrayList<ExtendedCommitInfo> foundCommitInfos = null;
		for (int i = 0; i < allCommitInfo.size(); i++) {
			ExtendedCommitInfo commitInfo = allCommitInfo.get(i);
			
			Date commitDate = commitInfo.getCommitDate();
		    double diffDays = getDiffDays(commitDate, openDate);
			
		    if (diffDays > pastDays) {
		    	continue;
		    }

	        if ((diffDays > 0) && (diffDays <= pastDays)) {
				if (null == foundCommitInfos) {
					foundCommitInfos = new ArrayList<ExtendedCommitInfo>();
				}

				foundCommitInfos.add(commitInfo);						
			} else {
				break;
			}			
		}
		
		return foundCommitInfos;
	}
}

