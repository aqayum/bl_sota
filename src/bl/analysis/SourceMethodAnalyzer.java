package bl.analysis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import bl.common.Bug;
import bl.common.Method;
import bl.config.Property;
import bl.db.AnalysisValue;
import bl.db.IntegratedAnalysisValue;
import bl.db.dao.BugDAO;
import bl.db.dao.IntegratedAnalysisDAO;
import bl.db.dao.MethodDAO;


/**
 * @author Abdul
 *
 */
public class SourceMethodAnalyzer {
	protected ArrayList<Bug> bugs;
	private ArrayList< Integer> methodIDs;
	private HashMap<Integer, HashMap<String, AnalysisValue>> methodAllTermMaps;
	private HashMap<Integer, Method> methodCorpusMap;
	private HashMap<Integer, Double> methodLengthScoreMap;
	
	public SourceMethodAnalyzer() {
		bugs = null;
		methodIDs = null;
		methodAllTermMaps = null;
		methodCorpusMap = null;
		methodLengthScoreMap = null;
	}
	
    public SourceMethodAnalyzer(ArrayList<Bug> bugs) {
    	this.bugs = bugs;
    	methodIDs = null;
    	methodAllTermMaps = null;
    	methodCorpusMap = null;
    	methodLengthScoreMap = null;
    }
    
	/**
	 * Calculate VSM score between source files and each bug report 
	 * 
	 */
	public void analyze(String version) throws Exception {
		MethodDAO methodDAO= new MethodDAO();
		methodIDs = methodDAO.getMethodIDs();
		methodAllTermMaps = new HashMap<Integer, HashMap<String, AnalysisValue>>();
		methodCorpusMap = new HashMap<Integer, Method>();
		methodLengthScoreMap = new HashMap<Integer, Double>();
		
		for(int i=0;i<methodIDs.size();i++) {
			int methodID = methodIDs.get(i);
			
			HashMap<String, AnalysisValue> methodTermMap = methodDAO.getTermMap(methodID);
			if (methodTermMap == null) {
				System.err.printf("Wrong Method ID: %d\n", methodID);
			}
			methodAllTermMaps.put(methodID, methodTermMap);
			
			Method method = methodDAO.getMethod(methodID);
			methodCorpusMap.put(methodID, method);
			
			double lengthScore = methodDAO.getLengthScore(methodID);
			methodLengthScoreMap.put(methodID, lengthScore);
		}
		
		ExecutorService executor = Executors.newFixedThreadPool(Property.THREAD_COUNT);

		for (int i = 0; i < bugs.size(); i++) {
			// calculate term count, IDC, TF and IDF
			Runnable worker = new WorkerThread(bugs.get(i), version);
			executor.execute(worker);
		}
		executor.shutdown();
		while (!executor.isTerminated()) {
		}
	}
	
    private class WorkerThread implements Runnable {
    	private Bug bug;
    	private String version;

    	
        public WorkerThread(Bug bug, String version){
            this.bug = bug;
            this.version = version;

        }
     
        @Override
        public void run() {
			// Compute similarity between Bug report & source methods
        	
        	try {

    				computeSimilarity(bug, version);

        	} catch (Exception e) {
        		e.printStackTrace();
        	}
        }
        
    	private void computeSimilarity(Bug bug, String version) throws Exception {
    		IntegratedAnalysisDAO integratedAnalysisDAO = new IntegratedAnalysisDAO();

    		MethodDAO methodDAO= new MethodDAO();
    		
    		BugDAO bugDAO = new BugDAO();
    		HashMap<String, AnalysisValue> bugMthTermMap = bugDAO.getMthTermMap(bug.getID());
    		Iterator<Integer> methodsTermsIter= methodAllTermMaps.keySet().iterator();
    		
    		while(methodsTermsIter.hasNext()) {
    			int methodID = methodsTermsIter.next();

    			// corpus, analysisValue
    			HashMap<String, AnalysisValue> methodTermMap = methodAllTermMaps.get(methodID);
    			if(methodTermMap!=null) {
    			double vsmClScore = 0.0;
    			double vsmScore = 0.0;
    			Iterator<String> methodTermIter = methodTermMap.keySet().iterator();
    			while (methodTermIter.hasNext()) {
    				String methodTerm = methodTermIter.next();
    				double methodTermWeightVSM_CL = methodTermMap.get(methodTerm).getTfClassic() * methodTermMap.get(methodTerm).getIdf();
    				double methodTermWeightVSM = methodTermMap.get(methodTerm).getTf() * methodTermMap.get(methodTerm).getIdf();
    				double bugTermWeightVSM_CL = 0;
    				double bugTermWeight = 0;
    				AnalysisValue bugTermValue = bugMthTermMap.get(methodTerm);
    				if (null != bugTermValue) {
    					bugTermWeightVSM_CL = bugTermValue.getTfClassic() * bugTermValue.getIdf();
    					bugTermWeight = bugTermValue.getTf() * bugTermValue.getIdf();
    					
    					// ASSERT code: IDF values must be same!
//    					if (bugTermValue.getIdf() != sourceFileTermMap.get(sourceFileTerm).getIdf()) {
//    						System.out.printf("Bug IDF: %f, Source IDF: %f\n", bugTermValue.getIdf(), sourceFileTermMap.get(sourceFileTerm).getIdf());
//    					}
    				} 
    				vsmClScore += (bugTermWeightVSM_CL * methodTermWeightVSM_CL);
    				vsmScore += (bugTermWeight * methodTermWeightVSM);
    			}

    			double methodNorm = methodDAO.getCopusNorm(methodID);
    			double bugNorm = bugDAO.getNormValue(bug.getID());
    			vsmScore = (vsmScore / (methodNorm * bugNorm));
    			vsmScore = vsmScore * methodLengthScoreMap.get(methodID);
    			
    			double methodNormCL = methodDAO.getCopusNormCL(methodID);
    			double bugNormCL = bugDAO.getNormValue(bug.getID());
    			vsmClScore = (vsmClScore / (methodNormCL * bugNormCL));
    			
    			
    			IntegratedAnalysisValue integratedAnalysisValue = new IntegratedAnalysisValue();
    			
    			integratedAnalysisValue.setBugID(bug.getID());
    			integratedAnalysisValue.setMethodID(methodID);
    			integratedAnalysisValue.setVsmScore(vsmScore);
    			integratedAnalysisValue.setVsmClassicScore(vsmClScore);
    			integratedAnalysisDAO.insertAnalysisVaule(integratedAnalysisValue);//
    			
    		}
    		}
    	}
    	

    }
}
