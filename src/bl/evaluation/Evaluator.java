package bl.evaluation;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import bl.common.Bug;
import bl.common.Method;
import bl.config.Property;
import bl.db.ExperimentResult;
import bl.db.IntegratedAnalysisValue;
import bl.db.dao.BugDAO;
import bl.db.dao.ExperimentResultDAO;
import bl.db.dao.IntegratedAnalysisDAO;
import bl.utils.Util;

/**
 * @author Abdul
 *
 */
public class Evaluator {

	
	protected ExperimentResult experimentResult;
	protected ArrayList<Bug> bugs = null;
	protected HashMap<Integer, HashSet<Method>> realFixedMethodsMap = null;;
	protected HashMap<Integer, ArrayList<IntegratedAnalysisValue>> rankedValuesMap = null;
	protected FileWriter writer = null; 
	
	protected Integer syncLock = 0;
	protected int top1 = 0;
	protected int top5 = 0;
	protected int top10 = 0;
	
	protected Double sumOfRRank = 0.0;
	protected Double MAP = 0.0;
	
	/**
	 * 
	 */
	public Evaluator(String productName, 
			double alpha,  int pastDays) {
		experimentResult = new ExperimentResult();
		experimentResult.setProductName(productName);

		experimentResult.setAlpha(alpha);

		experimentResult.setPastDays(pastDays);
		bugs = null;
		realFixedMethodsMap = null;
	}
	

	
	public void evaluate() throws Exception {
		long startTime = System.currentTimeMillis();
		System.out.printf("[STARTED] Evaluator.evaluate().\n");
		
		BugDAO bugDAO = new BugDAO();
		bugs = bugDAO.getAllBugs(true);
		
		realFixedMethodsMap = new HashMap<Integer, HashSet<Method>>();
		rankedValuesMap = new HashMap<Integer, ArrayList<IntegratedAnalysisValue>>();
		for (int i = 0; i < bugs.size(); i++) {
			int bugID = bugs.get(i).getID();
			HashSet<Method> fixedMethods = bugDAO.getFixedMethods(bugID);
			realFixedMethodsMap.put(bugID, fixedMethods);
			rankedValuesMap.put(bugID, getRankedValues(bugID, 0));
		}

		calculateMetrics();
		
		experimentResult.setExperimentDate(new Date(System.currentTimeMillis()));
		ExperimentResultDAO experimentResultDAO = new ExperimentResultDAO();
		experimentResultDAO.insertExperimentResult(experimentResult);
		
		System.out.printf("[DONE] Evaluator.evaluate().(Total %s sec)\n", Util.getElapsedTimeSting(startTime));
	}
	
	private ArrayList<IntegratedAnalysisValue> getRankedValues(int bugID, int limit) throws Exception {
		IntegratedAnalysisDAO integratedAnalysisDAO = new IntegratedAnalysisDAO();
		ArrayList<IntegratedAnalysisValue> rankedValues = null;

			rankedValues = integratedAnalysisDAO.getBugLocatorRankedValues(bugID, limit);
		
		return rankedValues;
	}
	
    private class WorkerThread implements Runnable {
    	private int bugID;
    	
        public WorkerThread(int bugID) {
            this.bugID = bugID;
        }
     
        @Override
        public void run() {
        	try {
        		calculateTopN();
        		calculateMRR();
        		calulateMAP();
        	} catch (Exception e) {
        		e.printStackTrace();
        	}
        }
        
        private void calculateTopN() throws Exception {
			HashSet<Method> realFixedMethods = realFixedMethodsMap.get(bugID);
			// Exception handling
			if (null == realFixedMethods) {
				return;
			}

			HashSet<Integer> fixedMethodIDs = new HashSet<Integer>();
			HashMap<Integer, Method> fixedMethodMap = new HashMap<Integer, Method>();

			Iterator<Method> fixedMethodsIter = realFixedMethods.iterator();
			while (fixedMethodsIter.hasNext()) {
				Method fixedMethod = fixedMethodsIter.next();
				fixedMethodIDs.add(fixedMethod.getID());
				fixedMethodMap.put(fixedMethod.getID(), fixedMethod);
			}
			
			int limitedCount = 10;
			
			// test code
			limitedCount = 30;
			
			ArrayList<IntegratedAnalysisValue> rankedValues = rankedValuesMap.get(bugID);
			if (rankedValues == null) {
				System.err.printf("[ERROR] Bug ID: %d\n", bugID);
				return;
			}
			for (int j = 0; j < rankedValues.size(); j++) {
				int sourceMethodID = rankedValues.get(j).getMethodID();
				if (fixedMethodIDs.contains(sourceMethodID)) {
					synchronized(syncLock) {
						if (j < 1) {
							top1++;
							top5++;
							top10++;
						
							String log = bugID + " " + fixedMethodMap.get(sourceMethodID).getName() + " " + (j + 1) + "\n";
							writer.write(log);
							break;						
						} else if (j < 5) {
							top5++;
							top10++;
							
							String log = bugID + " " + fixedMethodMap.get(sourceMethodID).getName() + " " + (j + 1) + "\n";
							writer.write(log);
							break;
						} else if (j < 10) {
							top10++;

							String log = bugID + " " + fixedMethodMap.get(sourceMethodID).getName() + " " + (j + 1) + "\n";
							writer.write(log);
							break;
						}
						// debug code
						else if (j < limitedCount) {
							String log = bugID + " " + fixedMethodMap.get(sourceMethodID).getName() + " " + (j + 1) + "\n";
							writer.write(log);
							break;
						}
					}
				}
			}
        }
        
        private void calculateMRR() throws Exception {
			HashSet<Method> fixedMethods = realFixedMethodsMap.get(bugID);
			// Exception handling
			if (null == fixedMethods) {
				return;
			}

			HashSet<Integer> fixedMethodIDs = new HashSet<Integer>();
			Iterator<Method> fixedMethodsIter = fixedMethods.iterator();
			while (fixedMethodsIter.hasNext()) {
				Method fixedMethod = fixedMethodsIter.next();
				fixedMethodIDs.add(fixedMethod.getID());
			}
			
			ArrayList<IntegratedAnalysisValue> rankedValues = rankedValuesMap.get(bugID);
			if (rankedValues == null) {
				System.err.printf("[ERROR] Bug ID: %d\n", bugID);
				return;
			}
			for (int j = 0; j < rankedValues.size(); j ++) {
				int sourceMethodID = rankedValues.get(j).getMethodID();
				
				if (fixedMethodIDs.contains(sourceMethodID)) {
//					System.out.printf("BugID: %s, Rank: %d\n", bugID, j + 1);
					synchronized(sumOfRRank) {
						sumOfRRank += (1.0 / (j + 1));
					}
					break;
				}
			}
        }
        
        private void calulateMAP() throws Exception {
        	double AP = 0;
        	
			HashSet<Method> fixedMethods = realFixedMethodsMap.get(bugID);
			// Exception handling
			if (null == fixedMethods) {
				return;
			}

			HashSet<Integer> fixedMethodIDs = new HashSet<Integer>();
			Iterator<Method> fixedMethodsIter = fixedMethods.iterator();
			while (fixedMethodsIter.hasNext()) {
				Method fixedMethod = fixedMethodsIter.next();
				fixedMethodIDs.add(fixedMethod.getID());
			}
			
			int numberOfFixedMethods = 0;
			int numberOfPositiveInstances = 0;
			ArrayList<IntegratedAnalysisValue> rankedValues = rankedValuesMap.get(bugID);
			if (rankedValues == null) {
				System.err.printf("[ERROR] Bug ID: %d\n", bugID);
				return;
			}
			for (int j = 0; j < rankedValues.size(); j++) {
				int sourceMethodID = rankedValues.get(j).getMethodID();
				if (fixedMethodIDs.contains(sourceMethodID)) {
					numberOfPositiveInstances++;
				}
			}

			double precision = 0.0;
			for (int j = 0; j < rankedValues.size(); j++) {
				int sourceMethodID = rankedValues.get(j).getMethodID();
				if (fixedMethodIDs.contains(sourceMethodID)) {
					numberOfFixedMethods++;
					precision = ((double) numberOfFixedMethods) / (j + 1);
					AP += (precision / numberOfPositiveInstances);
				}
			}
			
			synchronized(MAP) {
//				System.out.printf("[LOG]\t%d\t%f\n", bugID, AP);
				MAP += AP;
			}
        }
    }
    
    protected String getOutputFileName() {
		String outputFileName = String.format("./Results/%s_alpha_%.1f_%d",
				experimentResult.getProductName(), experimentResult.getAlpha(), experimentResult.getPastDays()); 
		
		outputFileName += "_" +".txt";
		
		return outputFileName;
    }
	
	protected void calculateMetrics() throws Exception {
		String outputFileName = getOutputFileName();
		writer = new FileWriter(outputFileName, false);
		
		ExecutorService executor = Executors.newFixedThreadPool(Property.THREAD_COUNT);
//		boolean isCounted = false;
		for (int i = 0; i < bugs.size(); i++) {
			Runnable worker = new WorkerThread(bugs.get(i).getID());
			executor.execute(worker);
		}
		
		executor.shutdown();
		while (!executor.isTerminated()) {
		}
		
		experimentResult.setTop1(top1);
		experimentResult.setTop5(top5);
		experimentResult.setTop10(top10);
		
		int bugCount = bugs.size();
		experimentResult.setTop1Rate((double) top1 / bugCount);
		experimentResult.setTop5Rate((double) top5 / bugCount);
		experimentResult.setTop10Rate((double) top10 / bugCount);

		System.out.printf("Top1: "+experimentResult.getTop1()+"\nTop5: "+ experimentResult.getTop5()+"\nTop10: "+experimentResult.getTop10()
				+"\nTop1Rate: "+experimentResult.getTop1Rate()+"\nTop5Rate: "+ experimentResult.getTop5Rate()+"\nTop10Rate: "+experimentResult.getTop10Rate());
		String log = "Top1: " + experimentResult.getTop1() + ", " +
				"Top5: " + experimentResult.getTop5() + ", " +
				"Top10: " + experimentResult.getTop10() + ", " +
				"Top1Rate: " + experimentResult.getTop1Rate() + ", " +
				"Top5Rate: " + experimentResult.getTop5Rate() + ", " +
				"Top10Rate: " + experimentResult.getTop10Rate() + "\n";
		writer.write(log);
		
////////////////////////////////////////////////////////////////////////////
		double MRR = sumOfRRank / bugs.size();
		experimentResult.setMRR(MRR);
		
		System.out.printf("MRR: %f\n", experimentResult.getMRR());
		log = "MRR: " + experimentResult.getMRR() + "\n";
		writer.write(log);

////////////////////////////////////////////////////////////////////////////
		MAP = MAP / bugs.size();
		experimentResult.setMAP(MAP);
		
		System.out.printf("MAP: %f\n", experimentResult.getMAP());
		log = "MAP: " + experimentResult.getMAP() + "\n";
		writer.write(log);
		
		writer.flush();
		writer.close();
	}
}
